## Look for the loss of heterozygosity in Aibi rare male

`/nfs/scistore03/vicosgrp/ukhaurat/w_snps_raremale`

`head_filtered2.vcf.simple_raremale` - simplified vcf
`head_filtered2.vcf.simple_raremale_nocov1` - simplified vcf without filtering for coverage
[vcf production](https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/raremale_w-snps.md#2-to-make-a-vcf-file-for-the-asexual-female-and-rare-male)

To calculate the number of heterozygous SNPs for each scaffold
```
cat head_filtered2.vcf.simple_raremale | awk '($10==1)' | awk '{print $1}' | sort | uniq -c | awk '{print $2, $1}'> hetrosites_F
cat head_filtered2.vcf.simple_raremale | awk '($11==1)' | awk '{print $1}' | sort | uniq -c | awk '{print $2, $1}'> hetrosites_M

cat Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt | awk '{print $1}'> scaf_on_LG
grep -f scaf_on_LG hetrosites_F > hetrosites_F_onlyonLGs
wc hetrosites_F
 43  86 927 hetrosites_F
wc hetrosites_F_onlyonLGs 
 6  12 134 hetrosites_F_onlyonLGs
```
So few!!!

Scaf | LG | heterosites F | heterosites M | log2 (F/M)  
<details><summary>CLICK ME to view the table</summary>
<p>

scaf | LG | heterosites F | heterosites M | log2 (F/M)
--- | --- | --- | --- | ---
tig00000030	| 2 | 584 | 404 | 0,5316130761  
tig00000036	| 6	| 34 | 37 | -0,1219905244  
tig00000062	| 6	| 1474 | 1482 | -0,007808923211  
tig00000072	| 8	| 2092 | 2133 | -0,02800111409  
tig00000099	| 8	| 529 | 528 | 0,002729792756  
tig00000105	| 2	| 1669 | 1357 | 0,2985632339 

</p>
</details> 


**I decided to do it on the vcf that isn't filtered for coverage**

```
cat head_filtered2.vcf.simple_raremale_nocov1 | awk '($10==1)' | awk '{print $1}' | sort | uniq -c | awk '{print $2, $1}'> hetrosites_F_nocov1
wc hetrosites_F_nocov1
 12810  25620 273801 hetrosites_F_nocov1
grep -f scaf_on_LG hetrosites_F_nocov1 > hetrosites_F_nocov_onlyonLGs
wc hetrosites_F_nocov_onlyonLGs
 1190  2380 26001 hetrosites_F_nocov_onlyonLGs
wc hetrosites_M_nocov_onlyonLGs
 1192  2384 26022 hetrosites_M_nocov_onlyonLGs
 
#two additional scaffolds!?
#to join the tables I leave only scaffolds present in hetrosites_F_nocov_onlyonLGs

cat hetrosites_F_nocov_onlyonLGs | awk '{print $1}' > minimal_scaf_set
grep -f minimal_scaf_set Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt > Scaffold_LGward_k40_assignmentOver5_NonAmbiguous_minimal_scaf_set.txt
grep -f minimal_scaf_set hetrosites_F_nocov_onlyonLGs_minset

#there are now 1190 scaffolds in each table

paste Scaffold_LGward_k40_assignmentOver5_NonAmbiguous_minimal_scaf_set.txt hetrosites_F_nocov_onlyonLGs_minset hetrosites_M_nocov_onlyonLGs -d " " | awk '{print $1, $2, $4, $6}' > Heterozygosity_for_scaf_nocov
```
Resulting file (Heterozygosity_for_scaf_nocov.txt): scaf | LG | #heterozygous SNPs for asex female | heterozygous SNPs for rare male |

I used the same R code to make a plot as for [coverage analysis](https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/Compare_genomic_coverage.md#4-illustrate-the-log2-femalerare-male-coverage-as-in-beatrizs-analysis-femalemale-coverage-asinica)  

```
het_comp<-read.table("~/Documents/raremale/Heterozygosity_for_scaf_nocov.txt", sep=" ", head=F)
colnames(het_comp) <- c("Scaf", "LG", "het_asexsis", "het_raremale")
head(het_comp)
plot(het_comp$LG, log2(het_comp$het_asexsis/het_comp$het_raremale), xlim=c(0,41), ylab="Log2(F/M_heterozygous_sites)", xlab="LG number", pch=20, col="grey")
abline(h=quantile(log2(het_comp$het_asexsis/het_comp$het_raremale), 0.5), lty=2, col="grey")
#LG to plot
i <- 1
while (i < 41) {
  print(i)
  xxx6<-subset(het_comp, LG==i)
  points(i, median(log2(xxx6$het_asexsis/xxx6$het_raremale), na.rm=T), col=pcol, pch=19)
  segments(i,quantile(log2(xxx6$het_asexsis/xxx6$het_raremale), 0.25), i, quantile(log2(xxx6$het_asexsis/xxx6$het_raremale), 0.75), col= pcol)
  i = i+1
}
```

![](images/heterozygous_sites_raremale.png)

**Trying to clean the graph a bit**
I want to leave only scaffolds that have at least 20 heterozygous sites in females

```
cat head_filtered2.vcf.simple_raremale_nocov1 | awk '($10==1)' | awk '{print $1}' | sort | uniq -c | awk '{print $2, $1}' | awk '($2>20)' > hetrosites_F_nocov1_more20
cat head_filtered2.vcf.simple_raremale_nocov1 | awk '($10==1)' | awk '{print $1}' | sort | uniq -c | awk '{print $2, $1}' | awk '($2>50)' > hetrosites_F_nocov1_more50
grep -f scaf_on_LG hetrosites_F_nocov1_more20 > hetrosites_F_nocov_more20_onlyonLGs
wc hetrosites_F_nocov_more20_onlyonLGs
1127  2254 24700 hetrosites_F_nocov_more20_onlyonLGs
wc hetrosites_F_nocov_more50_onlyonLGs
1040  2080 22873 hetrosites_F_nocov_more50_onlyonLGs

cat hetrosites_F_nocov_more20_onlyonLGs | awk '{print $1}' | awk '{gsub("_pilon", "");print}' > minimal_scaf_set_Fmore20
cat hetrosites_F_nocov_more50_onlyonLGs | awk '{print $1}' | awk '{gsub("_pilon", "");print}' > minimal_scaf_set_Fmore50
grep -f minimal_scaf_set_Fmore20 hetrosites_M_nocov1 > hetrosites_M_nocov1_more20Fset
grep -f minimal_scaf_set_Fmore20 Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt > Scaffold_LGward_k40_assignmentOver5_NonAmbiguous_minsetFmore20.txt
paste Scaffold_LGward_k40_assignmentOver5_NonAmbiguous_minsetFmore20.txt hetrosites_F_nocov_more20_onlyonLGs hetrosites_M_nocov1_more20Fset -d " " | awk '{print $1, $2, $4, $6}' > Heterozygosity_for_scaf_nocov_Fmore20

paste Scaffold_LGward_k40_assignmentOver5_NonAmbiguous_minsetFmore50.txt hetrosites_F_nocov_more50_onlyonLGs hetrosites_M_nocov1_more50Fset -d " " | awk '{print $1, $2, $4, $6}' > Heterozygosity_for_scaf_nocov_Fmore50

scp -o ProxyJump=ukhaurat@login.ist.ac.at ukhaurat@bea81:/nfs/scistore03/vicosgrp/ukhaurat/w_snps_raremale/Heterozygosity_for_scaf_nocov_Fmore20 /Users/ukhaurat/Documents/raremale/
```
![](images/heterozygosity_change_Fmore20.png)
There are some scaffolds showing significant drop in heterozygosity on the second LG! (on the graph log2(F/M number heterosites) increased, higher than 1) and this ration is overal increased for the second LG!

There is also a graph for scaffolds with more than 50 heterozygous sites in the female
<details><summary>CLICK ME to view the graph</summary>
<p>

 ![](images/q_heterozygosity_Fmore50_default.png)

</p>
</details> 

```
cat Heterozygosity_for_scaf_nocov_Fmore20 | awk '{print $1,$2,$3,$4, log($3/$4)/log(2)}' > Heterozygosity_for_scaf_nocov_Fmore20_log2
cat Heterozygosity_for_scaf_nocov_Fmore20_log2 | awk '($5>1)' > loss_of_heterozygosity_topScafs
grep -f top_scaf_lossheterozyg zw_snps_inraremalevcf.txt
``` 

5 out of 11 scaffolds showing the highest difference(decrease!) in heterozygosity are found in the list of ZW markers (their scaffolds) in the rare male.

