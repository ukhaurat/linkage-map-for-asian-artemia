# Linkage map construction for asian _Artemia spp_. from RNA-seq data 

**The data I'm using:**  
Raw RNA-seq files: `/archive3/group/vicosgrp/shared/Artemia.Sinica.Kazakh.Hybrids/SinicaMale.KazakhFemale/`
[list of locations of raw RNA-seq](https://docs.google.com/spreadsheets/d/1T1x13kDAKwl-ipFZFAvNBOqhvFKWg98eThHy62rcO6w/edit?usp=sharing)  
Draft genome *A.sinica*: `/archive3/group/vicosgrp/shared/ArtemiaProject/Genomes/Asinica_april2019/AsinicaMlongread/AsinicaMpbPilon.fasta`

RNA-seq data from F0 _Artemia sinica_ male and _Artemia kazakhstan_ female (inbreed-lines), 10 F2 backcrossed (to F0 _A.sinica_ male) hybrid females and 10 F2 backcrossed hybrid males.

<img src="images/true_scheme.png" width="500" > 

**The main aim:**  
To assign SNPs to linkage groups (autosomes and ZW sex chromosomes) and construct a linkage map for asian _Artemia spp_.

## Prerequisites

The programs I used:   
- `Picard/2.21.8/`
- `fastqc/0.11.7`
- `Trimmomatic-0.36` 
- `star/2.6.0c` 
- `samtools/1.8`
- `Lep-MAP3` set of modules (java) 

- All the files and scripts are placed:  `/nfs/scistore03/vicosgrp/ukhaurat/{data,scripts,proj}`

## Workflow

#####Processing of reads
1. **raw bams into fastq** The raw PE RNA-sequencing files were presented in bam, then they transformed into fastq files with `bam_into_fastq_rest.sh`.  
2. **fastqc** fastqc has been applied to resulting fastq files.  
*[each file (forward/reverse) contained from 30-40M reads of 150nt. There were warning messages in: Per base sequence content (for the first 10 nucleotides, Sequence Duplication Levels, and Overrepresented sequences); all of them are very typical for RNA-seq data.]*
3. before trimming I made a sample just for a trial
`zcat ../../data/sr.fastqc.gz | head -n 40000 > SRR_the same name.sample.fastq`
4. **trimming** The reads from fastq files were quality- and adapter- trimmed with `trimming_loop_mine.sh`.  
*[from each pair of files (PE) about 84% paiered reads survived, about 4% dropped, no adapters trimmed, overrepresented sequences are left (but it's normal for RNA-seq); trimming resulted in 4 files: forward trimmed reads, reverse, forward without a pair and reverse without a pair (ex. F2hybrid.female10_1.trim, F2hybrid.female10_1un.trim, F2hybrid.female10_2.trim, F2hybrid.female10_2un.trim)  
[trimming results](https://docs.google.com/document/d/1amokfpaVA14zSPbGD-63v5ClXnEwPO2wpjtlUw64exg/edit?usp=sharing)]*
5. **mapping** Trimmed reads were aligned to *A.sinica* draft genome with `star.sh`.  
*[around 70% of reads are uniquely mapped in each file, about 8% are mapped to multiple loci, 20% unmapped (too short), average mismatch rate - 1,2%    
[mapping results](https://docs.google.com/spreadsheets/d/1d0LvyZwdyg2QIk675OdFl-pwKgNlR3SULDHPWMaVAPQ/edit?usp=sharing)]*
##### Lep-MAP3
6. **file with genotype likelihoods** To get proper files with genotype likelihoods (post.gz) I used `SAMtools mpileup` as advised at
 [Lep_MAP3 page](https://sourceforge.net/p/lep-map3/wiki/LM3%20Home/). The script is `parallel_post_creation.sh`
*I also created files: sorted_bams(names of bam files for individuals in the correspondent order), mapping.txt(names of individuals), and contigs.txt(contig names),
used the provided scripts: pileupParser2.awk and pileup2posterior.awk, and made bai files (`samtools index *bam`).    
All these ateps are described at [Lep_MAP3 page](https://sourceforge.net/p/lep-map3/wiki/LM3%20Home/).*
7. **ParentCall2** 


## Scripts

#### bam_into_fastq_rest.sh

*Input:* raw sequencing results as bam files

- to transform raw bam files into fastq files

```
module load picard
cd /nfs/scistore03/vicosgrp/ukhaurat/proj
java -jar /nfs/scistore03/vicosgrp/Bioinformatics_2017/Reads/Programs/picard.jar SamToFastq SamToFastq I=F2_hybrid_male2.bam F=F2_hybrid_male2_1.fq F2=F2_hybrid_male2_2.fq
```

#### trimming_loop_mine.sh

*Input:* raw fastq files  

- to perform trimming by quality and adaptor trimming  

```
module load trimmomatic
module load java
cd /nfs/scistore03/vicosgrp/ukhaurat/proj

for f in /nfs/scistore03/vicosgrp/ukhaurat/proj/*_1.fq
do
base=$(basename ${f%%_*})
srun java -jar /nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 8 
${base}_1.fq ${base}_2.fq 
${base}_1.trim.fq ${base}_1un.trim.fq ${base}_2.trim.fq ${base}_2un.trim.fq 
ILLUMINACLIP:/nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 
SLIDINGWINDOW:4:22 MINLEN:36 LEADING:3 TRAILING:3
done
```
#### star.sh

*Input:* all the trimmed reads, fastq files (4 folders for each sample: forward, reverse, forward unpaired, reverse unpaired)

- to map/align redas to *A.sinica* draft genome.

```
module load star
cd  /nfs/scistore03/vicosgrp/ukhaurat/proj/

STAR --runMode genomeGenerate --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/proj/index/
--genomeFastaFiles /archive3/group/vicosgrp/shared/ArtemiaProject/Genomes/Asinica_april2019/AsinicaMlongread/AsinicaMpbPilon.fasta
--runThreadN 10 --genomeChrBinNbits 15

cd  /nfs/scistore03/vicosgrp/ukhaurat/proj/
for f in /nfs/scistore03/vicosgrp/ukhaurat/proj/*.fq
do base=$(basename ${f%%_*})
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/proj/index/ --readFilesIn ${base}_1.trim.fq ${base}_2.trim.fq
--outSAMtype BAM SortedByCoordinate
--outFileNamePrefix ${f/.fq/_mapped} --runThreadN 10
done
```

#### parallel_post_creation.gz

```
module load samtools

cd /nfs/scistore03/vicosgrp/ukhaurat/branch1_proj/
for i in $(cat contigs.txt)
do echo "samtools mpileup -r \"$i\" -q 10 -Q 10 -s \$(cat sorted_bams)|awk -f pileupParser2.awk|awk -f pileup2posterior.awk|gzip >\"$i\".post.gz"
done >SNP_calling.txt

parallel --jobs 16 < SNP_calling.txt

zcat *.post.gz | awk '(NR==1 || ($1!="CHR"))'|gzip >all_post_hybridH1.gz
```


#### ParentCall2.sh

*Input:* file with genotype likelihoods (post.gz) and pedigree.txt

- to call parental genotypes taking into account genotype information on parents and offspring / to identify markers.
```
cd /nfs/scistore03/vicosgrp/ukhaurat/proj/
zcat post.gz|java -cp /nfs/scistore03/vicosgrp/ukhaurat/software/bin/ ParentCall2 data=pedigree.txt posteriorFile=- removeNonInformative=1 |gzip >post.call.gz
```



## Some files

#### pedigree.txt
- this file illustrates the expected relatedness of individuals  
row1: name of the family   
row2: name of the individual  
row3: individuals' father   
row4: individuals' mother   
row5: sex (1-male, 2-female)  
row6: phenotype
```
CHR	POS	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F	F
CHR	POS	Gs1	Gk1	H1	f1	f2	f3	f4	f5	f6	f7	f8	f9	f10	m1	m2	m3	m4	m5	m6	m7	m8	m9	m10
CHR	POS	0	0	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1	Gs1
CHR	POS	0	0	Gk1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1	H1
CHR	POS	1	2	2	2	2	2	2	2	2	2	2	2	2	1	1	1	1	1	1	1	1	1	1
CHR	POS	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
```



## Authors

* **Uladzislava Khauratovich** - March, 2020
* Beatriz Vicoso


## License

This project is licensed under CopyLeft License - you can reproduce, modify, and use at will, as long as not for profity and always acknowleding the authors/collaborators.

## Acknowledgments

* This snippet was modified from the ReadMe.md from [@PurpleBooth on GitHub](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* Julia Raices