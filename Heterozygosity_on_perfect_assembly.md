# Heterozygosity on the perfect genome

For the paper Marwan improved the genome even better and now it is very good. So I want to redo heterozygosity on it.

### Mapping

First, I need to map the reads:

```
star_2.0.sh

module load star

cd /nfs/scistore03/vicosgrp/ukhaurat/perfect_genome_heterozygosity_raremale/
STAR --runMode genomeGenerate --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/perfect_genome_heterozygosity_raremale/index/ --genomeFastaFiles /nfs/scistore03/vicosgrp/melkrewi/genome_assembly_december_2021/43.realign_reads/plot/Artemia_sinica_genome_29_12_2021.fasta --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/perfect_genome_heterozygosity_raremale/index/ --readFilesIn raremale_Aibi_1_trimmed.fq raremale_Aibi_2_trimmed.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/raremale_Aibi_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/perfect_genome_heterozygosity_raremale/index/ --readFilesIn asexualsister_Aibi_1_trimmed.fq asexualsister_Aibi_2_trimmed.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/asexsister_Aibi_ --runThreadN 10

sbatch star_perfect.sh 
Submitted batch job 28039093

```

### VCF creation

- vcf creation with the less strict filtering for coverage. 
(here)[https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/5.raremale_w-snps_new_genome.md#2a-to-make-a-vcf-file-filer-for-coverage-5]

```
ln -s /nfs/scistore03/vicosgrp/melkrewi/genome_assembly_december_2021/43.realign_reads/plot/Artemia_sinica_genome_29_12_2021.fasta Artemia_sinica_genome_29_12_2021.fasta
```

```
cd /nfs/scistore03/vicosgrp/ukhaurat/perfect_genome_heterozygosity_raremale/

#Call SNPs from the BAM alignments
srun bcftools mpileup -a AD,DP,SP -Ou -f Artemia_sinica_genome_29_12_2021.fasta asexsister_Aibi_Aligned.sortedByCoord.out.bam raremale_Aibi_Aligned.sortedByCoord.out.bam | bcftools call -v -f GQ,GP -mO z -o head_asex_raremale.vcf.gz

#filter for quality and coverage
srun vcftools --gzvcf head_asex_raremale.vcf.gz --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 5 --max-meanDP 100 --minDP 5 --maxDP 100 --recode --stdout > head_asex_raremale_cov5_filtered.vcf

#Filter 2: remove multiallelic
bcftools view --max-alleles 2 --exclude-types indels head_asex_raremale_cov5_filtered.vcf > head_asex_raremale_cov5_filtered2.vcf

sbatch snp_calling_asex_raremale.sh 
Submitted batch job 28040999
```

create a simplified vcf:
```
cat head_asex_raremale_cov5_filtered2.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_asex_raremale_cov5_filtered2.vcf_simple
```
why is it so big (100 858 148)? in comparisson with `head_Gk1_Gs1_f_m_filtered2.vcf_simple` (10 064 250 bytes). oh! probably it is because DNA was used.

