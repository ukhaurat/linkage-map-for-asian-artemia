### The location of _A.franciscana_ homologous scaffolds on our cluster-LGs

Beatriz prepared a file where she mapped _A.franciscana_ genes (which are sorted to Z, youngZ, Autosome) to the reference _A.sinica_ genome and found homologous scaffolds in it [AfranCDS_assign_AsinLocation.txt](https://git.ist.ac.at/bvicoso/artemia_zw_sexasex_2020/-/blob/master/Afranciscana_Asinica_homology/data/AfranCDS_assign_AsinLocation.txt?expanded=true&viewer=simple)  

```
#The file looks like
#A.franciscana_gene/group/A.sinica_scaffold
ArtemiaFranciscana_1003439 NA tig00008985_pilon
ArtemiaFranciscana_1004732 Autosome tig00012184_pilon
ArtemiaFranciscana_1005577 Autosome tig00012496_pilon
ArtemiaFranciscana_1005621 Autosome tig00006800_pilon
ArtemiaFranciscana_1005996 Autosome tig00041938_pilon
ArtemiaFranciscana_1006242 Autosome tig00037758_pilon
```

#### I extracted the homologous scaffolds for each category and located them on our cluster_LGs
```
grep -w [Z||youngZ||Autosome] Afranciscana_Asinica_homology_data_AfranCDS_assign_AsinLocation.txt > grep_[Z||youngZ||Autosomes]_scaf
  
cat grep_Z_scaf | awk '{print $3}' | awk '{gsub("_pilon", "");print}' > Z_fran_scaf_simple
grep -f Z_fran_scaf_simple Scaffold_LGward_k40_assignmentOver5_simple.txt > Z_fran_scaf_inLGs
cat Z_fran_scaf_inLGs | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forZfran
# I also did for NonAmbiguous LGs
grep -f Z_fran_scaf_simple Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forZfran_NonAmbiguous
```

| ![ks](images/Z_Franciscana_Scaffolds_on_cluster-LGs.png) | ![ks](images/Z_Franciscana_Scaffolds_on_cluster-LGs__only_NonAmbiguous__.png) | 
| :--: | :--: |
   
![ks](images/youngZ_Franciscana_Scaffolds_on_cluster-LGs__the_same_for_NonAmbiguous_-3.png)  

YoungZ scaffolds are also located on the second cluster-LG (there is only one scaffold on LG 23)
   
#### I also placed Autosome scaffolds on LGs and and the hybrid scaffolds (that were used for the linkage map construction) to compare its number on each LG.
```
cat Scaffold_LGward_k40_assignmentOver5_simple.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forall
grep -f Autosome_fran_scaf_simple Scaffold_LGward_k40_assignmentOver5.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forAutosomefran
#and for NonAmbiguous LGs
cat Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forall_NonAmb
grep -f Autosome_fran_scaf_simple Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forAutosomefran_NonAmb
```
![ks](images/A.sinica_Scaffolds__from_hybrid_cross__and_the_homologous_A.franciscana_scaffolds_on_cluster-LGs-3.png)  
![ks](images/A.sinica_Scaffolds__from_hybrid_cross__and_the_homologous_A.franciscana_scaffolds_on_cluster-LGs__NonAmbiguous_scaffolds_.png)
