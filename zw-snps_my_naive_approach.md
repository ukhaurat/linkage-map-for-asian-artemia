# Linkage groups and ZW-SNPs  
02.07.2020

**The data I'm using:**  

</nfs/scistore03/vicosgrp/ukhaurat/admixture/head_filtered2_mbams.vcf> - vcf with single merged bams for grandparents


## Workflow
### Preliminary steps

1. I merged bam files (sequenced twice) for grandparents

```
java -jar $PICARD MergeSamFiles I=Gs1.sinica.male.v1_sortedByCoord.bam I=Gs1.sinica.male.v2_sortedByCoord.bam O=Gs1.sinica.male.both_sortedByCoord.bam
```
2. The vcf has been previosly filtered.
(from [Beatriz's admixture pipeline](https://git.ist.ac.at/bvicoso/bioinformaticsii/-/blob/master/Population%20Genomics/Admixture.md))
```
#filter for quality and coverage
srun vcftools --gzvcf head.vcf.gz  --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 100 --minDP 10 --maxDP 100 --recode --stdout >  head_filtered.vcf

#Filter 2: remove multiallelic
bcftools view --max-alleles 2 --exclude-types indels head_filtered.vcf > head_filtered2_mbams.vcf
```
3. I created a new simplified vcf:
```
cat head_filtered2_mbams.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_filtered2.vcf.simple_new
/nfs/scistore03/vicosgrp/ukhaurat/admixture/head_filtered2.vcf.simple_new
```
4. Then I filtered markers that are specific for A.kazakhstan (GM != 0, GF = 0) and that are heterozygous in [5-15] individuals (out of 20)   
as it was done [Beatriz's pipeline](https://git.ist.ac.at/bvicoso/randomdataanalysis/-/blob/master/SlavaLepMap_2020/ImproviseLGmap.md)   

```
cat head_filtered2.vcf.simple_new | grep -v 'NA' | awk '($30 != "0" && $31 == "0")' |  cut -d " " -f1,10-29 | grep -v '\W2\W' | awk '( ($1+$2+$3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20)>4 && ($1+$2+$3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20)<16)' > head_filtered2.vcf.simple_new.filtered
```
*I’ve got 5747 unique GT combinations after bams for grandparents were merged instead of 4123 (Beatriz got)
found at least 3 times: 1170  (instead of 850)*   

5. I also made the corresponding files:  
`Scaffold_LGward_k40_assignmentOver5.txt` - scaffold/LG (more than 5 markers from a scaffold on a LG)  
`Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt` - only scaffolds where these >5 markers are only on one LG  
```
cat Scaffold_LGward_k40.txt | perl -pi -e 's/\.[0-9]* / /gi' | sort | uniq -c | awk '($1>5)' | awk '{print $2, $3}' > Scaffold_LGward_k40_assignmentOver5.txt
only unambiguous scaffolds
cat Scaffold_LGward_k40.txt | perl -pi -e 's/\.[0-9]* / /gi' | sort | uniq -c | awk '($1>5)' | awk '{print $2, $3}' | awk '{print $1}' | sort | uniq -c | awk '($1==1)' | awk '{print $2}' | sort | join /dev/stdin Scaffold_LGward_k40_assignmentOver5.txt > Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt
```
### Main steps
6. I've got markers that segregate exactly like W-SNPs (from kazakh), the list of **70 ZW scaffolds**.
```
cat head_filtered2.vcf.simple_new.filtered | awk '( ($2+$3+$4+$5+$6+$7+$8+$9+$10+$11)>9 && ($12+$13+$14+$15+$16+$17+$18+$19+$20+$21) == 0)' > head_filtered2.vcf.simple.filtered_zw_strict
cat head_filtered2.vcf.simple.filtered_zw_strict |awk '{print $1}'| uniq > zw_scaffolds.txt
```
7. I checked where my ZW scaffolds are situated on new-obtained-cluster LGs (scaffolds with only >5 markers on a LG).  
most of them on **the 2nd LG**
```
awk '{gsub("_pilon", "");print}' zw_scaffolds.txt > zw_scaffolds_simple.txt  ##to remove _pilon from each line
grep -f zw_scaffolds_simple.txt Scaffold_LGward_k40_assignmentOver5.txt > myzwinOver5.txt
cat myzwinOver5.txt | awk '{print $2}'| sort |uniq -c
#number of scaffolds/LG
      1 1
     36 2
      1 5
      1 6

```
8. I also checked how many markers are on each LGs.
```
grep -f zw_scaffolds_simple.txt Scaffold_LGward_k40_new.txt > zw_scaffolds_inLGs
cat zw_scaffolds_inLGs | awk '{print $2}'| sort |uniq -c > markersnumber_LG_myzw.txt
#number of markers/LG
      8 1
      1 14
      2 19
    977 2
      2 21
      1 3
      1 31
      2 4
     19 5
     37 6
```
we will consider only LGs with more than 5 markers on each.  

![ks](images/number_of_ZW_markers_on_each_LG.png)

#### * all my zw scaffolds are found in top_perl_zwScaffolds from [Beatriz's results with perl script](https://git.ist.ac.at/bvicoso/randomdataanalysis/-/blob/master/SlavaLepMap_2020/FindZWsnps_june2020.md)

```
grep -f perl_topzwScaffolds.txt zw_scaffolds.txt > topzwPerl_in_myzw.txt

awk '{gsub("_pilon", "");print}' perl_topzwScaffolds.txt > perl_topzwScaffolds_simple.txt
#i deleted emty lines!
sed '/^$/d' perl_topzwScaffolds_simple.txt > perl_topzwScaffolds_simple1.txt
grep -f perl_topzwScaffolds_simple1.txt Scaffold_LGward_k40_assignmentOver5.txt > perlzwinOver5.txt
cat perlzwinOver5.txt | awk '{print $2}'| sort |uniq -c
      1 1
     25 2
      1 6
```
Hooray! most are on **the 2nd LG**
___
___

#### mix with Marwan's results

1. I've extracted ZW scaffolds from Marwan's analysis (unique scaffolds from the file `Pilon_vs_dataset2.txt`), as I understood this is the file with reads unique for hybrid females)  
there were **50 ZW scaffolds**
```
cat Pilon_vs_dataset2.txt | awk '{print $14}'| sort| uniq > zw_Pilons_Marwan
```
2. I checked for common ZW scaffolds from mine and Marwan's analysis:
```
comm -12  <(sort zw_Pilons_Marwan) <(sort zw_scaffolds.txt)
zw_comm.txt
```
i found 28 common scaffolds in our results

3. Then I searched for LGs for Marwan's zw scaffolds (i call them pilons) and counted the number of markers on each found LG `LGsforzwPilons.txt`
```
awk '{gsub("_pilon", "");print}' zw_Pilons_Marwan > zw_Pilons_Marwan_simple.txt
grep -f zw_Pilons_Marwan_simple.txt Scaffold_LGward_k40_new.txt | > zw_Pilons_inLGs.txt
cat zw_Pilons_inLGs.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forzwPilons.txt
( echo -e "#ofMarkers\LG"; cat markersnumber_LG_forzwPilons.txt ) >LGsforzwPilons.txt
```
we will consider only LGs with more than 5 markers on each.  

![ks](images/number_of_ZW_markers_from_Marwan_s_scaffolds_on_each_LG.png)  
it seems LGs: 2,23 (18,27) can be parts of sex chromosomes

___

##Questions 02/07/2020:

1. A lot of NAs for GM (kazakh) genotype in the vcf, what it can be connected with?
.  
2. 


___
## Authors

* **Uladzislava Khauratovich**
* Beatriz Vicoso
* Marwan Elkrewi
