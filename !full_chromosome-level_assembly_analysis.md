# Analysis on the new chromosome-level genome assembly of _Artemia sinica_. 

I want to repeat some analysis using a new genome assembly created by Marwan. 

## 1. Map 20 hybrids RNA and their grandparents to the new genome.

Working directory: `/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/`.    
Genome: `/nfs/scistore03/vicosgrp/melkrewi/Project_confirm_genome_assembly/CHRR_integrated.fa`.      
Trimmed RNA reads from 20 hybrids and Kazakhstan and sinica grandparents: `/nfs/scistore03/vicosgrp/ukhaurat/dummyfather_proj/trimmed`.   

- make links of files with trimmed reads
cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/
ln -s /nfs/scistore03/vicosgrp/ukhaurat/dummyfather_proj/trimmed/*fq ./

- map trimmed reads to the new full genome

```
star_2.0.sh

module load star

cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/
STAR --runMode genomeGenerate --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --genomeFastaFiles /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/CHRR_integrated.fa --runThreadN 10 --genomeChrBinNbits 15

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female10_1.trim.fq F2hybrid.female10_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female10_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female9_1.trim.fq F2hybrid.female9_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female9_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female8_1.trim.fq F2hybrid.female8_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female8_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female7_1.trim.fq F2hybrid.female7_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female7_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female6_1.trim.fq F2hybrid.female6_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female6_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female5_1.trim.fq F2hybrid.female5_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female5_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female4_1.trim.fq F2hybrid.female4_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female4_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female3_1.trim.fq F2hybrid.female3_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female3_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female2_1.trim.fq F2hybrid.female2_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female2_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.female1_1.trim.fq F2hybrid.female1_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.female1_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male1_1.trim.fq F2hybrid.male1_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male1_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male2_1.trim.fq F2hybrid.male2_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male2_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male3_1.trim.fq F2hybrid.male3_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male3_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male4_1.trim.fq F2hybrid.male4_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male4_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male5_1.trim.fq F2hybrid.male5_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male5_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male6_1.trim.fq F2hybrid.male6_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male6_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male7_1.trim.fq F2hybrid.male7_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male7_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male8_1.trim.fq F2hybrid.male8_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male8_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male9_1.trim.fq F2hybrid.male9_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male9_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn F2hybrid.male10_1.trim.fq F2hybrid.male10_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/F2hybrid.male10_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn Gk1.kaz.female.v1_1.trim.fq Gk1.kaz.female.v1_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/Gk1.kaz.female.v1_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn Gk1.kaz.female.v2_1.trim.fq Gk1.kaz.female.v2_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/Gk1.kaz.female.v2_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn Gs1.sinica.male.v1_1.trim.fq Gs1.sinica.male.v1_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/Gs1.sinica.male.v1_ --runThreadN 10
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn Gs1.sinica.male.v2_2.trim.fq Gs1.sinica.male.v2_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/Gs1.sinica.male.v2_ --runThreadN 10

sbatch star_2.0.sh
Submitted batch job 24431142
```
#### Mapping statistics
tip ion making mapping statistics
```
for file in /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/mapping_stats/*.final.out; do cat $file | grep "Uniquely mapped reads %"; done
```

## 2. Merge v1 and v2 bams for grandparents 

```
i merged bams for kazakhstan and sinica grandparents (made 1 out of two):
java -jar $PICARD MergeSamFiles -I Gk1.kaz.female.v1_Aligned.sortedByCoord.out.bam -I Gk1.kaz.female.v2_Aligned.sortedByCoord.out.bam -O Gk1.kaz.female.both_Aligned.sortedByCoord.out.bam

java -jar $PICARD MergeSamFiles -I Gs1.sinica.male.v1_Aligned.sortedByCoord.out.bam -I Gs1.sinica.male.v2_Aligned.sortedByCoord.out.bam -O Gs1.sinica.male.both_Aligned.sortedByCoord.out.bam
```
     

## 3. Create a vcf file

I created a vcf file for 20 F2 hybrids and merged bams for grandparents. I used [ADMIXTURE pipeline](https://git.ist.ac.at/bvicoso/bioinformaticsii/-/blob/master/Population%20Genomics/Admixture.md#2-snp-calling-using-samtools-bcftools-and-vcftools)  

Here and there the vcf is filtered:
* for minimum and maximum coverage
* for minimum quality
* indels and multi-allelic sites were removed

```
module load samtools
module load bcftools
module load vcftools

cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/

#Call SNPs from the BAM alignments
srun bcftools mpileup -a AD,DP,SP -Ou -f CHRR_integrated.fa Gk1.kaz.female.both_Aligned.sortedByCoord.out.bam Gs1.sinica.male.both_Aligned.sortedByCoord.out.bam F2hybrid.female1_Aligned.sortedByCoord.out.bam F2hybrid.female2_Aligned.sortedByCoord.out.bam F2hybrid.female3_Aligned.sortedByCoord.out.bam F2hybrid.female4_Aligned.sortedByCoord.out.bam F2hybrid.female5_Aligned.sortedByCoord.out.bam F2hybrid.female6_Aligned.sortedByCoord.out.bam F2hybrid.female7_Aligned.sortedByCoord.out.bam F2hybrid.female8_Aligned.sortedByCoord.out.bam F2hybrid.female9_Aligned.sortedByCoord.out.bam F2hybrid.female10_Aligned.sortedByCoord.out.bam F2hybrid.male1_Aligned.sortedByCoord.out.bam F2hybrid.male2_Aligned.sortedByCoord.out.bam F2hybrid.male3_Aligned.sortedByCoord.out.bam F2hybrid.male4_Aligned.sortedByCoord.out.bam F2hybrid.male5_Aligned.sortedByCoord.out.bam F2hybrid.male6_Aligned.sortedByCoord.out.bam F2hybrid.male7_Aligned.sortedByCoord.out.bam F2hybrid.male8_Aligned.sortedByCoord.out.bam F2hybrid.male9_Aligned.sortedByCoord.out.bam F2hybrid.male10_Aligned.sortedByCoord.out.bam | bcftools call -v -f GQ,GP -mO z -o head_Gk1_Gs1_f_m.vcf.gz

#filter for quality and coverage
srun vcftools --gzvcf head_Gk1_Gs1_f_m.vcf.gz --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 100 --minDP 10 --maxDP 100 --recode --stdout > head_Gk1_Gs1_f_m_filtered.vcf

#Filter 2: remove multiallelic
bcftools view --max-alleles 2 --exclude-types indels head_Gk1_Gs1_f_m_filtered.vcf > head_Gk1_Gs1_f_m_filtered2.vcf

sbatch snp_calling_F2hybrids.sh 
Submitted batch job 24431683
```
     

## 4. Filtering the VCF file and detecting sex-linked scaffolds

- simplify the vcf.   

(pipeline)[https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/4.zw-snps_perl_script.md#simplify-the-vcf] 
To simplify it so 0/0 is replaced with 0, 0/1 with 1, and 1/1 with 2.

```
cat head_Gk1_Gs1_f_m_filtered2.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_Gk1_Gs1_f_m_filtered2.vcf_simple
```
the resulting file `head_Gk1_Gs1_f_m_filtered2.vcf_simple` (10 064 250 bytes) from a vcf `head_Gk1_Gs1_f_m_filtered2.vcf` (109 719 574)
for example `head_filtered2_G_f.vcf.simple` (10 622 274), `head_filtered2_G_f.vcf` (110 376 890)

- search for W- and ZW-snps.  

(pipeline)[https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/zw-snps_with_Beatrizs_perl_script.md#filtering-the-vcf-file-and-detecting-sex-linked-scaffolds]

We want to know which of our SNPs are sex-linked. Here are the possible chromosomal assignments:
* class "0" is autosomal (it was not alternatively fixed in males and females)
* class "1" is a W-SNP (it was found in all the F2 females and none of the males, and it was heterozygous in the inbread grandmother)
* class "2" is a ZW-SNP (it was found in all the F2 females and none of the males, but it was homozygous in the ZW grandmother) 
* class "3" is Z-specific (it was homozygous in grandmother and not passed on to F2) [NB: this is not found in the data!]

To run our script, we will use:
* The simplified VCF file head_Gk1_Gs1_f_m_filtered2.vcf_simple
* The script [FindZWSNPs_v2.pl](scripts/FindZWSNPs_v2.pl) to filter the vcf

The columns for females, males and grandparents must be specified in the script (first individual = 0).  
I changed the original FindZWSNPs_v2.pl into /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/FindZWSNPs_v2_changed.pl
```
@female_columns = (2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
@male_columns = (12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
$grannycolumn = 0;
$granpacolumn = 1;
```

[FindZWSNPs_v2_.pl](scripts/FindZWSNPs_v2.pl) does two things:
1.  it filters out SNPs:
    * that are 0 or NA in grandmother
    * that are 1, 2 or NA in grandfather (since they should have the sinica allele)
    * that are 2 or NA in the F2
    * that are 1 in fewer than 4 or more than 15 F2 individuals
2. It classifies SNPs into autosomal, W-linked, ZW or Z-specific (see above).

I am running the script:
```
perl FindZWSNPs_v2_changed.pl head_Gk1_Gs1_f_m_filtered2.vcf_simple
```

This produces the file head_Gk1_Gs1_f_m_filtered2.vcf_simple.ZWassign, which has the following columns (separated by spaces, I hate tabs now):
* scaffold 
* position 
* grandmother GT (1 = het, 2 = hom)
* number of heterozygous F2 females 
* number of heterozygous F2 males 
* chromosomal assignment (autosome "0", W chrom "1", sex chromosomes "2") 
 
__Quick first look at the results `head_Gk1_Gs1_f_m_filtered2.vcf_simple.ZWassign`__

We can already have a quick look at how many SNPs get classified as W, ZW, or Z-specific:
```
0 [autosome] 54841
1 [W] 181
2 [ZW] 820
```

```
cat head_Gk1_Gs1_f_m_filtered2.vcf_simple.ZWassign | awk '($6==1)' > w_snps_perl_script_full_assembly.txt
cat head_Gk1_Gs1_f_m_filtered2.vcf_simple.ZWassign | awk '($6==2)' > zw_snps_perl_script_full_assembly.txt
```





## 7. Make a new vcf with the correct Gs1

It figured out Gs1 version was broken. I redid it and merged bams for two versions again.  
```
java -jar $PICARD MergeSamFiles -I Gs1.sinica.male.v1_Aligned.sortedByCoord.out.bam -I Gs1.sinica.male.v2_Aligned.sortedByCoord.out.bam -O Gs1.sinica.male.both_Aligned.sortedByCoord.out.bam
```
  
Then I called SNPs for this merged bam for Gs1 and decided to merge old vcf with this one:
```
module load samtools
module load bcftools
module load vcftools

cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/

#Call SNPs from the BAM alignments
srun bcftools mpileup -a AD,DP,SP -Ou -f CHRR_integrated.fa Gs1.sinica.male.both_Aligned.sortedByCoord.out.bam | bcftools call -v -f GQ,GP -mO z -o head_Gs1.vcf.gz

#filter for quality and coverage
srun vcftools --gzvcf head_Gs1.vcf.gz --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 100 --minDP 10 --maxDP 100 --recode --stdout > head_Gs1_filtered.vcf

#Filter 2: remove multiallelic
bcftools view --max-alleles 2 --exclude-types indels head_Gs1_filtered.vcf > head_Gs1_filtered2.vcf
``` 

Merge two vcf files the old made with the brocken Gs1.bam 'head_Gk1_Gs1_f_m_filtered2.vcf' and the new just for the correct Gs1 'head_Gs1_filtered2.vcf.gz'.  

merge_Gs1_and_all_VCFs_try_again.sh
```
# merge command works only with bgzipped files (not gzip!), thus I compressed the vcfs:

cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/

module load samtools

bgzip -c head_Gk1_Gs1_f_m_filtered2.vcf > head_Gk1_Gs1_f_m_filtered2.vcf.gz
tabix -p vcf head_Gk1_Gs1_f_m_filtered2.vcf.gz
bgzip -c head_Gs1_filtered2_try_again.vcf > head_Gs1_filtered2_try_again.vcf.gz
tabix -p vcf head_Gs1_filtered2_try_again.vcf.gz

module load bcftools

bcftools merge --force-samples head_Gk1_Gs1_f_m_filtered2.vcf.gz head_Gs1_filtered2_try_again.vcf.gz > head_Gs1_merged_try_again.vcf

#however the outbut is uncompressed by default
#if i put the vcf containing all the individuals the outbut is bigger then input and all the individuals preserved (however if one-individual vcf goes first, only one is left in the output)
#--force-samples means: `Merge multiple VCF/BCF files from non-overlapping sample sets to create one multi-sample file. For example, when merging file A.vcf.gz containing samples S1, S2 and S3 and file B.vcf.gz containing samples S3 and S4, the output file will contain four samples named S1, S2, S3, 2:S3 and S4` ! acctually, 2:S3 happened to be after all of other columns
#Finally, I renamed Gs1.sinica.male.both_Aligned.sortedByCoord.out.bam into Gs1_new.sinica.male.both_Aligned.sortedByCoord.out.bam in 'here head_Gs1_filtered2_try_again.vcf' to see which column is from the correct version.  
it seemed 2:Gs1 had less information however had to be from the correct file.

sbatch merge_Gs1_and_all_VCFs.sh 
Submitted batch job 24569303
```

`head_Gs1_merged_try_again.vcf` needed file 
column Gs1.sinica.male.both_Aligned.sortedByCoord.out.bam must be ignored.
NAMES OF THE COLUMNS
```
#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  Gk1.kaz.female.both_Aligned.sortedByCoord.out.bam       Gs1.sinica.male.both_Aligned.sortedByCoord.out.bam      F2hybrid.female1_Aligned.sortedByCoord.out.bam  F2hybrid.female2_Aligned.sortedByCoord.out.bam  F2hybrid.female3_Aligned.sortedByCoord.out.bam  F2hybrid.female4_Aligned.sortedByCoord.out.bam  F2hybrid.female5_Aligned.sortedByCoord.out.bam  F2hybrid.female6_Aligned.sortedByCoord.out.bam  F2hybrid.female7_Aligned.sortedByCoord.out.bam  F2hybrid.female8_Aligned.sortedByCoord.out.bam  F2hybrid.female9_Aligned.sortedByCoord.out.bam  F2hybrid.female10_Aligned.sortedByCoord.out.bam F2hybrid.male1_Aligned.sortedByCoord.out.bam    F2hybrid.male2_Aligned.sortedByCoord.out.bam    F2hybrid.male3_Aligned.sortedByCoord.out.bam    F2hybrid.male4_Aligned.sortedByCoord.out.bam    F2hybrid.male5_Aligned.sortedByCoord.out.bam    F2hybrid.male6_Aligned.sortedByCoord.out.bam    F2hybrid.male7_Aligned.sortedByCoord.out.bam    F2hybrid.male8_Aligned.sortedByCoord.out.bam    F2hybrid.male9_Aligned.sortedByCoord.out.bam    F2hybrid.male10_Aligned.sortedByCoord.out.bam   Gs1_new.sinica.male.both_Aligned.sortedByCoord.out
```
---------------------------------------------------------
I did some other stuff    
-tried to compare vcfs by sites and by individuals   
-tried to remove the wrong Gs1 but it did not work!    

```
module load samtools
module load vcftools

cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/

#vcftools --vcf head_Gk1_Gs1_f_m_filtered2.vcf --diff head_Gs1_merged_try.vcf --diff-site
#vcftools --vcf head_Gk1_Gs1_f_m_filtered2.vcf --diff head_Gs1_merged_try.vcf --diff-indv --out diff_indv_vcf
vcftools --vcf head_Gs1_merged_try_again.vcf --remove-indv Gs1.sinica.male.both_Aligned.sortedByCoord.out.bam > head_merged_Gs1_newonly_try_again.vcf
```

- Finally, I just called SNPs again from all the correct files: Gk1both, Gs1both, f1-10, m1-10. `head_Gk1_Gs1_f_m_filtered2.vcf`
      

## 8. Check the distance between markers within and between LGs in R

1. simplify a new VCF with the correct Gs1

i simplified the merged file
```
cat head_Gs1_merged_try_again.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_filtered2_merged_try_again.vcf.simple
```
i simplified the new file (with snps called from scratch from correct files)
```
cat head_Gk1_Gs1_f_m_filtered2.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_Gk1_Gs1_f_m_filtered2.vcf.simple
```

__work with the file `head_Gk1_Gs1_f_m_filtered2.vcf.simple`__  

2. Filtering for

-correct grandparental genotypes (Gk1 different from A.sinica reference !="0" means 1 or 2; Gs1 the same as in A.sinica =="0")   
-not ALL sites are homozygous or heterozygous    

```
cat head_Gk1_Gs1_f_m_filtered2.vcf.simple | grep -v 'NA' | awk '($10 != "0" && $11 == "0")' |  cut -d " " -f1,12-31 | grep -v '\W2\W' | awk '( ($2+$3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21)>4 && ($2+$3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21)<16)' > head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered
```
```
scp -o ProxyJump=ukhaurat@login.ist.ac.at ukhaurat@bea81:/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered /Users/ukhaurat/Documents/full_assembly_analysis
```

3. Distance between markers for each LG in R

NB: the code worked fine on the cluster, but gave a memory error on my laptop. I had to give R more memory before running it: Sys.setenv('R_MAX_VSIZE'=32000000000)

R code to calculate the distance between markers (patterns of segragating SNPs) within LGs and among all the SNPs.
```
setwd("/Users/ukhaurat/Documents/full_assembly_analysis")
xxx<-read.table("head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered", head = F, sep = " ")

#rename rows and remove first row, otherwise it gets confused when calculating distance
row.names(xxx)<-make.names(gsub("_pilon", "", xxx$V1), unique=TRUE)
xxx<-xxx[,2:21]
#manhattan distance for x1,y1 and x2,y2 is |x1-x2|+|y1-y2|, which I think is what we want
distmatrix<-dist(xxx, method = "manhattan")

###compare distance between and within clusters
par(mfrow=c(1,1))
par(lwd=2)
plot(1, median(distmatrix, na.rm=T), xlim=c(0.5,23), ylim=c(0,20), col="blue", ylab="Distance", pch=20, axes=FALSE, xlab="")
abline(h=median(distmatrix, na.rm=T), lty=2, col="blue")

Axis(side=1, at=c(-1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23), labels=c("", "All", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22"), tick = F)
segments(-1,0,22, 0, lwd=1)
Axis(side=2, labels=T)
segments(1,quantile(distmatrix,0.25), 1, quantile(distmatrix,0.75), col="blue")


Packages = c("dplyr" , "tidyr", "data.table" , "readxl", "ggplot2", "plotly");
invisible(lapply(Packages, library, character.only = TRUE))

library(stringr)
my_table<-read.table("head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered", head = F, sep = " ")


my_table$V1 <- gsub("^.{0,2}", "", my_table$V1)
my_table <- mutate(my_table, V1 = if_else(str_detect(`V1`, "affold_"), "22", V1))
my_table$V1 <- as.numeric(my_table$V1)


#LG to plot
i <- 1
while (i < 24) {
  print(i)
  
  xxx2<-subset(my_table,V1==i)
  row.names(xxx2)<-make.names(xxx2$V1, unique=TRUE)
 
  xxx2<-xxx2[,2:21]
  distmatrix2<-dist(xxx2, method = "manhattan")
  points(i+1, median(distmatrix2, na.rm=T), col="red", pch=20)
  segments(i+1,quantile(distmatrix2, 0.25), i+1, quantile(distmatrix2, 0.75), col="red")
  
  i = i+1
}
```
![](images/Distance_within_and_between_LGs.png)   
*the distance between markers (patterns of segragating SNPs) within LGs (red) and among all the SNPs (blue). NL - distance between SNPs from scaffolds not assigned to any of LGs. The wiskers represent distribution between 25th and 75th percentiles*
     

## 9. Visualize the (manhattan) distance between markers on LG6

Extract LG6 from the simplified VCF and leave the column with the coordinates  
```
cat head_Gk1_Gs1_f_m_filtered2.vcf.simple | grep -v 'NA' | awk '($10 != "0" && $11 == "0")' | awk '($1 == "LG6")' | cut -d " " -f2,12-31 | grep -v '\W2\W' | awk '( ($2+$3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21)>4 && ($2+$3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21)<16)' > head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered_LG6plus
```
```
scp -o ProxyJump=ukhaurat@login.ist.ac.at ukhaurat@bea81:/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered_LG6plus /Users/ukhaurat/Documents/full_assembly_analysis
```
Code in R based on the simplified vcf file for LG6.
```
setwd("/Users/ukhaurat/Documents/full_assembly_analysis")

library(stringr)
my_table<-read.table("head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered_LG6plus", head = F, sep = " ")
row.names(my_table)<-my_table$V1

#my_table$V1 <- gsub("^.{0,2}", "", my_table$V1)
#my_table <- mutate(my_table, V1 = if_else(str_detect(`V1`, "affold_"), "22", V1))
#my_table$V1 <- as.numeric(my_table$V1)
#xxx2<-subset(my_table,V1=="LG6")

xxx<-my_table[,2:21]
#manhattan distance for x1,y1 and x2,y2 is |x1-x2|+|y1-y2|, which I think is what we want
distobject<-dist(xxx, method = "manhattan")

distmatrix <- as.matrix(distobject)
range(distmatrix, na.rm = FALSE)
heatmap(distmatrix, scale="column", Rowv=NA, Colv=NA, symm=TRUE, col=heat.colors(18), xlab="coordinates of some markers", ylab="coordinates of some markers", ColSideColors=heat.colors(3807), main="Distance between markers LG6")

#legend(x="bottomright", legend=c("min", "ave", "max"), fill=colorRampPalette(brewer.pal(8, "Oranges"))(3))
```
![](images/Distance_between_markers_google_slides.png)

On this heatmap there are too much yellow. it must be distance 5 in average, however it looks like 10.


We want to do the heatmap for LG3 (it has lower average distance betwen markers) to see the difference

```
cat head_Gk1_Gs1_f_m_filtered2.vcf.simple | grep -v 'NA' | awk '($10 != "0" && $11 == "0")' | awk '($1 == "LG3")' | cut -d " " -f2,12-31 | grep -v '\W2\W' | awk '( ($2+$3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21)>4 && ($2+$3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21)<16)' > head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered_LG3plus

scp -o ProxyJump=ukhaurat@login.ist.ac.at ukhaurat@bea81:/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered_LG3plus /Users/ukhaurat/Documents/full_assembly_analysis
```

| ![LG3](images/Screenshot_2021-07-08_at_14.06.45.png) | ![100markers](images/Screenshot_2021-07-08_at_14.02.27.png) | 
| :--: | :--: |
| heatmap fpr LG3 | heatmap for just 100 markers from LG3 |  

**It seems heatmap R doesn't depict the colorcode correctly, for example, it used different red-orange for distance 10 in different rows in the same heatmap**

## 10. Heatmap for the whole genome

`head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered3`
filtered for      
-correct grandparental genotypes.   
-got rid of NAs.    
-not all sites heterozygous/homozygous.   
-i lest all the columns.  
-i removed grep -v '\W2\W' -\_O-O_/- who knows what it is...??

```
cat head_Gk1_Gs1_f_m_filtered2.vcf.simple | grep -v 'NA' | awk '($10 != "0" && $11 == "0")' | awk '{print $1, $2, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30, $31}' | grep -v '\W2\W' | awk '( ($3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21+$22)>4 && ($3+$4+$5+$6+$7+$8+$9+$10+$11+$12+$13+$14+$15+$16+$17+$18+$19+$20+$21+$22)<16)' > head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered

scp -o ProxyJump=ukhaurat@login.ist.ac.at ukhaurat@bea81:/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered /Users/ukhaurat/Documents/full_assembly_analysis
```
  

### 10a. The heatmap for 10% of the markers (random sample) from each chromosome. 
 
```
'R_MAX_VSIZE'=32000000000
setwd("/Users/ukhaurat/Documents/full_assembly_analysis")

library(dplyr)
library(stringr)
df<-read.table("head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered", header = F, sep = " ", col.names = c("Chromosome", "Coordinate", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8", "m9", "m10"))
df <- mutate(df, add_name = if_else(str_detect(`Chromosome`, "scaffold_"), "NoLG", Chromosome))
df <- df[!grepl("scaffold",df$Chromosome),]
df$add_name <- gsub("^.{0,2}", "", df$add_name)
df$add_name <- as.numeric(df$add_name)

#make a toy table
df_sample <- df %>% group_by(add_name) %>%
sample_frac(size = 0.1, replace = TRUE) %>%
arrange(add_name, by_group = TRUE)

library(tidyr)
#this part just to create coordinate lables for the ggplot
df_sample$num <- seq(1,3864,1)
breakss <- as.vector(as.numeric(seq(1,3864,50)))
b<-subset(df_sample,df_sample$num %in% breakss)
#b<-unite(b, "Label", Chromosome, Coordinate, sep = "_", remove = TRUE)
labelss <- as.vector(as.character(b$Chromosome))

xxx<-df_sample[,3:22]
#manhattan distance for x1,y1 and x2,y2 is |x1-x2|+|y1-y2|, which I think is what we want
distobject<-dist(xxx, method = "manhattan")

library(usethis)
library(devtools)
library(spaa)

distdf <- spaa::dist2list(distobject)
write.table(distdf, file = "/Users/ukhaurat/Documents/full_assembly_analysis/distdf_0.1_NoNoLG_2", append = FALSE, quote = TRUE, sep = " ", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE)

#distdf<-read.table("distdf_0.1")

distdf$row <- as.numeric(distdf$row)
distdf$col <- as.numeric(distdf$col)

library(ggplot2)
library(hrbrthemes)
library(scico)

svg("heatmap_genome_0.1.svg")

ggplot(distdf, aes(row, col, fill= value)) + 
  geom_tile(aes(fill = value), colour = "transparent") +
  scale_fill_gradient(trans = "log", low = "dark green", high = "white", breaks = c(0, 2, 5, 10, 20)) +
  labs(title = "Distance between markers genome 0.1 sample") + 
  scale_x_continuous(breaks = breakss, labels = labelss) + 
  scale_y_continuous(breaks = breakss, labels = labelss) +
  theme(axis.text.x= element_text(size=6, angle=45)) + 
  theme(axis.text.y = element_text(size=6))

# Close the graphics device
dev.off() 
```
![](images/Heatmap_0.1_genome_disent_labels_NoNoLGs.png)  

### 10b. The heatmap for the whole genome.

To calculate matrix for the whole genome more computational power is needed. Thus, I create a slurm script for the cluster

Slurm sript
```
#!/bin/bash
#
#SBATCH --job-name=heatmap
#SBATCH --output=STAR.%j.out
#SBATCH --error=STAR.%j.err
#SBATCH -c 12
#SBATCH --time=96:00:00
#SBATCH --mem=350G
#SBATCH --no-requeue
#SBATCH --partition=defaultp
#SBATCH --mail-type=ALL
#SBATCH --mail-user=slava.khovratovich@gmail.com
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/

module load R

Rscript heatmap_genome.R
```

R script
```
setwd("/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/")

library(dplyr)
library(stringr)
df<-read.table("head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered", header = F, sep = " ", col.names = c("Chromosome", "Coordinate", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8", "m9", "m10"))
df <- mutate(df, add_name = if_else(str_detect(`Chromosome`, "scaffold_"), "NoLG", Chromosome))


library(tidyr)
#this part just to create coordinate lables for the ggplot
df$num <- seq(1,45573,1)
breakss <- as.vector(as.numeric(seq(1,45573,1000)))
b<-subset(df,df$num %in% breakss)
#b<-unite(b, "Label", Chromosome, Coordinate, sep = "_", remove = TRUE)
labelss <- as.vector(as.character(b$add_name))

xxx<-df[,3:22]
#manhattan distance for x1,y1 and x2,y2 is |x1-x2|+|y1-y2|, which I think is what we want
distobject<-dist(xxx, method = "manhattan")

library(usethis)
library(devtools)
library(spaa)

distdf <- spaa::dist2list(distobject)
write.table(distdf, file = "/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/distdf", append = FALSE, quote = TRUE, sep = " ",
            eol = "\n", na = "NA", dec = ".", row.names = TRUE,
            col.names = TRUE)
write.csv(distdf, file = "/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/distdf.csv")

distdf$row <- as.numeric(distdf$row)
distdf$col <- as.numeric(distdf$col)

library(ggplot2)

pdf("heatmap_genome.pdf")
svg("heatmap_genome.svg")

ggplot(distdf, aes(row, col, fill= value)) + 
  geom_tile() +
  scale_fill_gradient(low="red", high="yellow") +
  labs(title = "Distance between markers") + 
  scale_x_continuous(breaks = breakss, labels = labelss) + 
  scale_y_continuous(breaks = breakss, labels = labelss) +
  theme(axis.text.x= element_text(size=6, angle=45)) + 
  theme(axis.text.y = element_text(size=6, angle=45))

# Close the graphics device
dev.off()
```
i cannot get the figure, however matrix is calculated :(

```
setwd("/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/")

library(dplyr)
library(stringr)
df<-read.table("head_Gk1_Gs1_f_m_filtered2.vcf.simple.filtered", header = F, sep = " ", col.names = c("Chromosome", "Coordinate", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "m1", "m2", "m3", "m4", "m5", "m6", "m7", "m8", "m9", "m10"))
df <- mutate(df, add_name = if_else(str_detect(`Chromosome`, "scaffold_"), "NoLG", Chromosome))

library(tidyr)
#this part just to create coordinate lables for the ggplot
df$num <- seq(1,45573,1)
breakss <- as.vector(as.numeric(seq(1,45573,1000)))
b<-subset(df,df$num %in% breakss)
#b<-unite(b, "Label", Chromosome, Coordinate, sep = "_", remove = TRUE)
labelss <- as.vector(as.character(b$add_name))

library(usethis)
library(devtools)
library(spaa)

distdf<-read.table("/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/distdf", header = T)

distdf$row <- as.numeric(distdf$row)
distdf$col <- as.numeric(distdf$col)

library(ggplot2)

svg("heatmap_genome.svg")

ggplot(distdf, aes(row, col, fill= value)) + 
  geom_tile() +
  scale_fill_gradient(low="red", high="yellow") +
  labs(title = "Distance between markers") + 
  scale_x_continuous(breaks = breakss, labels = labelss) + 
  scale_y_continuous(breaks = breakss, labels = labelss) +
  theme(axis.text.x= element_text(size=6, angle=45)) + 
  theme(axis.text.y = element_text(size=6, angle=45))


dev.off()

sbatch R_heatmap_reduced.sh 
Submitted batch job 24828046
```
