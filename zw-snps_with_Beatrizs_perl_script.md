## Detecting sex-linked scaffolds with Beatriz's perl script

07.07.2020  
Here I will repeat the analysis [Beatriz did](https://git.ist.ac.at/bvicoso/randomdataanalysis/-/blob/master/SlavaLepMap_2020/FindZWsnps_june2020.md) but with merged bam files for grandparents: `/nfs/scistore03/vicosgrp/ukhaurat/admixture/head_filtered2_mbams.vcf` - vcf with single merged bams for grandparents

  
# Filtering the VCF file and detecting sex-linked scaffolds

*24/06/2020 update: fixed bug that made it impossible to find Z-specific SNPs, as they are 0 in all individuals, but were selected after we filtered for min number of 1 = 5.*

*This did not change the results as we still have no Z-specific SNPs, sad.*


## Get the file

Slava already produced a VCF file:
/nfs/scistore03/vicosgrp/ukhaurat/admixture/head_filtered2_all.vcf

This has been filtered:
* for minimum and maximum coverage
* for minimum quality
* indels and multi-allelic sites were removed

## Simplify the VCF

Before we get started: we only need the genotypes. We can also simplify it so 0/0 is replaced with 0, 0/1 with 1, and 1/1 with 2, as I find this easier to parse.

For the time being, I will ignore the second replicates of the grandparents, as this will be then easier to apply to the final VCF made from the combined BAM.

_Later Slava produced a new vcf with merged bam replicates for grandparents and made the same operations with this: `head_filtered2_mbams.vcf`_

```
#DON'T DO THIS
##cat head_filtered2_all.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_filtered2.vcf.simple
##that is giving me the weirdest error, with columns getting randomly deleted. 

#DO THIS INSTEAD
#turning into space-separated file seems to work, but I have no idea why. GRRRRR.
cat head_filtered2_all.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_filtered2.vcf.simple
```

Here is what the simplified VCF looks like:
```
#CHROM POS ID REF ALT QUAL FILTER INFO FORMAT F2hybrid.female1_sortedByCoord.bam [...] Gs1.sinica.male.v2_sortedByCoord.bam
tig00000008_pilon 88648 . G A 234 . . GT 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 1 0 1 NA NA 1 1
tig00000008_pilon 88722 . G C 273 . . GT 1 0 0 0 1 0 0 0 0 0 0 0 0 1 1 1 0 1 1 1 NA NA 1 1
tig00000008_pilon 90124 . G A 162 . . GT 1 0 1 0 1 1 0 0 0 0 1 0 1 0 0 0 1 1 1 0 NA NA 0 1
tig00000008_pilon 90243 . C T 999 . . GT 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 NA NA 1 1
tig00000008_pilon 107493 . A G 40.5043 . . GT 1 1 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 0 0 NA NA 0 0
tig00000008_pilon 107506 . A G 588 . . GT 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 NA NA 1 1
tig00000008_pilon 107507 . G T 51 . . GT 1 1 1 0 0 0 1 0 0 0 0 0 1 0 1 0 0 0 0 0 NA NA 0 0
```

## Filter and assign SNPs to chromosomal classes

We want to know which of our SNPs are sex-linked. Here are the possible chromosomal assignments:
* class "0" is autosomal (it was not alternatively fixed in males and females)
* class "1" is a W-SNP (it was found in all the F2 females and none of the males, and it was heterozygous in the inbread grandmother)
* class "2" is a ZW-SNP (it was found in all the F2 females and none of the males, but it was homozygous in the ZW grandmother) 
* class "3" is Z-specific (it was homozygous in grandmother and not passed on to F2) [NB: this is not found in the data!]


To run our script, we will use:
* The simplified VCF file head_filtered2.vcf.simple
* The script [FindZWSNPs_v2.pl](scripts/FindZWSNPs_v2.pl) to filter the vcf

The script assumes the following order for the individuals (first individual = 0)
```
F2 females are in columns : (0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
F2 males are in columns : (10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
The grandmother is in column 20;
The grandfather is in column 22.
```
Once we have the final VCF, we will only have one column per grandparent, so **we will need to fix this in the perl script**.
_Slava did it for the new vcf_

[FindZWSNPs_v2.pl](scripts/FindZWSNPs_v2.pl) does two things:
1.  it filters out SNPs:
    * that are 0 or NA in grandmother
    * that are 1, 2 or NA in grandfather (since they should have the sinica allele)
    * that are 2 or NA in the F2
    * that are 1 in fewer than 4 or more than 15 F2 individuals
2. It classifies SNPs into autosomal, W-linked, ZW or Z-specific (see above).


To run the script:
```
perl FindZWSNPs_v2.pl head_filtered2.vcf.simple
```

This produces the file head_filtered2.vcf.simple.ZWassign, which has the following columns (separated by spaces, I hate tabs now):
* scaffold 
* position 
* grandmother GT (1 = het, 2 = hom)
* number of heterozygous F2 females 
* number of heterozygous F2 males 
* chromosomal assignment 
 

## Quick first look at the results

We can already have a quick look at how many SNPs get classified as W, ZW, or Z-specific:
```
cat head_filtered2.vcf.simple.ZWassign | awk '{print $6}' | sort | uniq -c | awk '{print $2, $1}'
0 [autosome] 44299
1 [W] 201
2 [ZW] 805
```
For the new vcf file `head_filtered2.vcf.simple_mbams.ZWassign` there are a bit less SNPs found:
```
0 [autosome] 41187
1 [w] 113
2 [zw] 637
```

And let's get the list of scaffolds that have at least 10 SNPs suggestive of sex-linkage: 
```
cat head_filtered2.vcf.simple.ZWassign | awk '($6>0)' | awk '{print $1}' | sort | uniq -c | awk '{print $2, $1}' | awk '($2>9)'
```

The current table of ZW scaffolds is:

Scaffold | Sexlinked_SNP_count | W_count | ZW_count | Auto
--- | --- | --- | --- | ---
tig00000030_pilon | 27 | 2 | 25 | 0
tig00000087_pilon | 62 | 9 | 53 | 28
tig00000105_pilon | 26 | 2 | 24 | 3
tig00000208_pilon | 16 | 8 | 8 | 31
tig00000375_pilon | 86 | 12 | 74 | 1
tig00001243_pilon | 29 | 20 | 9 | 6
tig00001304_pilon | 10 | 0 | 10 | 26
tig00001379_pilon | 23 | 13 | 10 | 14
tig00002281_pilon | 77 | 3 | 74 | 12
tig00002493_pilon | 12 | 4 | 8 | 3
tig00002938_pilon | 20 | 10 | 10 | 0
tig00003074_pilon | 15 | 1 | 14 | 0
tig00003428_pilon | 62 | 0 | 62 | 3
tig00003877_pilon | 11 | 0 | 11 | 2
tig00005209_pilon | 11 | 0 | 11 | 21
tig00005791_pilon | 32 | 9 | 23 | 5
tig00007249_pilon | 21 | 2 | 19 | 4
tig00009620_pilon | 32 | 1 | 31 | 10
tig00010694_pilon | 63 | 2 | 61 | 11
tig00010847_pilon | 45 | 14 | 31 | 5
tig00012001_pilon | 14 | 0 | 14 | 0
tig00013906_pilon | 12 | 1 | 11 | 1
tig00013943_pilon | 11 | 0 | 11 | 0
tig00015734_pilon | 10 | 1 | 9 | 3
tig00016896_pilon | 53 | 24 | 29 | 24
tig00041910_pilon | 36 | 1 | 35 | 16
tig00042647_pilon | 12 | 11 | 1 | 10
tig00373061_pilon | 10 | 2 | 8 | 18
tig00373678_pilon | 22 | 6 | 16 | 4


From the new file there is only a subset (from the first analysis, 24 out of 29) of zw scaffolds identified:

Scaffold | Sexlinked_SNP_count
--- | ---
tig00000030_pilon | 12
tig00000087_pilon | 55
tig00000105_pilon | 30
tig00000375_pilon | 45
tig00001243_pilon | 14
tig00001379_pilon | 20
tig00002281_pilon | 61
tig00002493_pilon | 14
tig00003074_pilon | 14
tig00003428_pilon | 64
tig00003877_pilon | 16
tig00007249_pilon | 21
tig00009620_pilon | 15
tig00010694_pilon | 34
tig00010847_pilon | 36
tig00012001_pilon | 21
tig00013906_pilon | 12
tig00013943_pilon | 11
tig00015734_pilon | 10
tig00016896_pilon | 41
tig00016969_pilon | 10
tig00041910_pilon | 26
tig00373061_pilon | 15
tig00373678_pilon | 19
