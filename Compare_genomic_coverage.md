## Compare genomic coverage between raremale_Aibi and his parthenogenetic sister

#### 1. I downloaded and processed DNA for raremale_Aibi and his parthenogenetic sister  

> _Both individuals are progeny of the same female:_  
> _8493 99732 RARE MALE! DNA HiSeqV4 PE125 Whole Body Genome 0, colony isolated for dissections Mel 6/11/2019_  
> `/archive3/group/vicosgrp/shared/Artemia.Aibi/99732_CCTGTTCAGAGAACAA_CDV9PANXX_6_20190927B_20190927.bam`  
> _8493 99733 Female DNA HiSeqV4 PE125 Whole Body Genome 0, colony isolated for dissections Mel 6/11/2019_  
> `/archive3/group/vicosgrp/shared/Artemia.Aibi/99733_CCACACTGTGTGAATC_CDV9PANXX_6_20190927B_20190927.bam`  

I trimmed the reads and mapped them to _A.sinica_ genome
`/archive3/group/vicosgrp/shared/ArtemiaProject/Genomes/Asinica_april2019/AsinicaMlongread/AsinicaMpbPilon.fasta`  
Around 76% of reads survived after trimming, from them around 40% of the reads are uniquely mapped, 18% of reads mapped to multiple loci.

[trimming and mapping results](https://docs.google.com/spreadsheets/d/1fJiXKhBuFj9qwXUPU4GpXMAWsGItBW1YUM0IvpS4KcI/edit?usp=sharing)

#### 2. Then with deepTools bamCompare I created bedgraph files with log2 coverage ratio asexual_female/raremale in bins(windows) of 50, 2000 and 5000 bases.  

Example of a bedgraph file
```
/nfs/scistore03/vicosgrp/ukhaurat/raremale/Compare_asexual_raremale.bedgraph

tig00000002_pilon       150     200     0.0376221
tig00000002_pilon       200     250     2.58496
tig00000002_pilon       250     300     2.32193
tig00000002_pilon       300     350     2.80735
tig00000002_pilon       350     400     1.58496
tig00000002_pilon       400     450     2.35955
tig00000002_pilon       450     500     2.62258
```
*It's possible to get the analogous bigwig files and visualize them (each scaffold) in IGV  
*I also created files with the lists of scaffolds for each LG here: `/nfs/scistore03/vicosgrp/ukhaurat/raremale/scaffolds_for_each_LG`

#### 3. To unify the method used I calculated coverage with the tool Vincent used: bowtie2 and soap/coverage  

```
module load bowtie2
module load soap/coverage

cd /nfs/scistore03/vicosgrp/ukhaurat/raremale/

#Build the indexed transcriptome for Bowtie2
srun bowtie2-build AsinicaMpbPilon.fasta bwindex

#Mapping fastq files of each library to the reference genome with Bowtie2
bowtie2 -x bwindex -1 asexualsister_Aibi_1_trimmed.fq -2 asexualsister_Aibi_2_trimmed.fq --end-to-end --sensitive -p 8 -S asexualsister_Aibi.sam
bowtie2 -x bwindex -1 raremale_Aibi_1_trimmed.fq -2 raremale_Aibi_2_trimmed.fq --end-to-end --sensitive -p 8 -S raremale_Aibi.sam

#Get only uniquely mapped reads for each library
grep -vw "XS:i" asexualsister_Aibi.sam > asexualsister_Aibi_unique.sam
grep -vw "XS:i" raremale_Aibi.sam > raremale_Aibi_unique.sam

#Compute the DNA coverage with soap coverage for each library
soap.coverage -sam -cvg -i asexualsister_Aibi_unique.sam -onlyuniq -p 8 -refsingle AsinicaMpbPilon.fasta -o asexualsister_Aibi_unique.soapcov
soap.coverage -sam -cvg -i raremale_Aibi_unique.sam -onlyuniq -p 8 -refsingle AsinicaMpbPilon.fasta -o raremale_Aibi_unique.soapcov
```
#### 4. Illustrate the log2 female/rare male coverage as in [Beatriz's analysis female/male coverage _A.sinica_](https://git.ist.ac.at/bvicoso/artemia_zw_sexasex_2020/-/blob/master/Coverage/PlotCov_LGs.md)

1.Clean the SOAPcov outputs:  
`cat asexualsister_Aibi_unique.soapcov | perl -pi -e 's/_pilon://gi' | perl -pi -e 's/:/ /gi' | perl -pi -e 's/\// /gi' | awk '{print $1,$3,$7}' | sort > asexualsister_unique_scaf.length.cov1.soapcov`

2.Merge tables for further R analysis and plot  
`sort Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt | join /dev/stdin asexualsister_unique_scaf.length.cov1.soapcov | join /dev/stdin raremale_unique_scaf.length.cov1.soapcov | awk '{print $1, $2, $3, $4, $6}' > AsinicaGenome_asexsis_raremale.txt`  

Resulting file (AsinicaGenome_asexsis_raremale.txt): scaf | LG | coordinate | asexualsister_cov | raremale_cov |  

3.Plot in R:

```
cov_comp<-read.table("~/Documents/raremale/AsinicaGenome_asexsis_raremale.txt", sep=" ", head=F)
colnames(cov_comp) <- c("Scaf", "LG", "Length", "asexsis", "raremale")
head(cov_comp)
plot(cov_comp$LG, log2(cov_comp$asexsis/cov_comp$raremale), ylab="Log2(F/Mcov)", xlab="LG number", pch=20, col="grey")
abline(h=quantile(log2(cov_comp$asexsis/cov_comp$raremale), 0.5), lty=2, col="grey")
#LG to plot
i <- 1
while (i < 41) {
  print(i)
  xxx5<-subset(cov_comp, LG==i)
  points(i, median(log2(xxx5$asexsis/xxx5$raremale), na.rm=T), col=pcol, pch=19)
  segments(i,quantile(log2(xxx5$asexsis/xxx5$raremale), 0.25), i, quantile(log2(xxx5$asexsis/xxx5$raremale), 0.75), col= pcol)
  i = i+1
}
```

The plot looks like:

![](images/asexsis_raremale_cov_on_LGs.png)  

#### Results: We couldn't find any sighnificant difference in coverage for any linkage group. However, there is a tail of scaffolds with increased and decreased coverage on the second LG.

*5th and 19th LGs are absent. There are no NonAmbiguous scaffolds on them in `Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt`  

#### 5. I also extracted scaffolds with the increased or decreased F/M log2 coverage ratio.  

These scaffolds mostly form the tail on the 2nd LG.
```
#The sixth column is added with ratio values:  
cat AsinicaGenome_asexsis_raremale.txt | awk '{print $1,$2,$3,$4,$5, log($4/$5)/log(2)}' > AsinicaGenome_asexsis_raremale_covratio3.txt
```
  
Then I created files with scaffolds showing increased (more than 0.2 | 0.18) or decreased (less than -0.2 | -0.22) F/M coverage:  
```
AsinicaGenome_asexsis_raremale_covratio_lessthanminus0.2.txt  

scaf | LG | coordinate | asexualsister coverage (F) | rare male coverage (M) | log2(F/M) |
tig00000105 2 978608 9.3 11 -0.242201
tig00001243 2 475501 7.6 8.9 -0.227806
tig00011499 36 123542 6 6.9 -0.201634
tig00016969 2 39704 6.1 7.3 -0.259087
tig00020407 23 24336 7.2 8.3 -0.205114
tig00022248 23 29244 6.1 7.2 -0.239188
tig00026091 39 26232 4.2 4.9 -0.222392
tig00029601 24 16736 5.2 6.5 -0.321928
tig00373477 31 221815 6.7 7.7 -0.200697
tig00373678 2 199614 8.8 11 -0.321928
tig00374702 10 13579 4.8 5.6 -0.222392  

AsinicaGenome_asexsis_raremale_covratio_morethan0.2.txt  

tig00003215 2 249723 9.5 8.2 0.212304
tig00012104 39 88360 11 9.4 0.226771
tig00013906 2 72929 12 8.6 0.480626
tig00015734 2 66443 11 9.1 0.273565
tig00038736 30 21775 6.5 5.6 0.215013
```