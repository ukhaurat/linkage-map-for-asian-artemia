# W-SNPs in females and males of all avaliable species

We want to check for W-SNPs in males and females of different species. We will use RNA from heads of males and female. It will allow to have a representation how sex chromosomes are different in A.sinica, A.franciscana, A.urmiana, and A.kazakhastan and how the level of differentiation is different in them.
We will also check for W-SNPs in parthenogenetic (asexual) species which is going to prove they have the same sex chromosomes.

working directory: `/nfs/scistore03/vicosgrp/ukhaurat/all_species_males_females_Wsnps`   

## 1. Transform raw bam files with RNA reads into fasta

Location of all the raw reads [Beatriz's table](https://git.ist.ac.at/bvicoso/artemia_zw_sexasex_2020/-/blob/master/UsefulData_July2021.md#rna-seq-data-head)

<details><summary>code for bam -> fastq transformation</summary>
<p>

```
bam_to_fastq

module load picard

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaSinica/39897_CCGTCC_C9HBBANXX_5_20160809B_20160809.bam -F A.sinica.F.head.RNA.97_1.fq -F2 A.sinica.F.head.RNA.97_2.fq

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaSinica/39898_GCCAAT_C9HBBANXX_5_20160809B_20160809.bam -F A.sinica.F.head.RNA.98_1.fq -F2 A.sinica.F.head.RNA.98_2.fq

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/arartemia_RNAseq_reads/ArtemiaSinica/40769_ACTGAT_C9MF6ANXX_1_20160830B_20160830.bam -F A.sinica.F.head.RNA.69_1.fq -F2 A.sinica.F.head.RNA.69_2.fq

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaSinica/40770_GTCCGC_C9MF6ANXX_1_20160830B_20160830.bam -F A.sinica.F.head.RNA.70_1.fq -F2 A.sinica.F.head.RNA.70_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaSinica/39901_TGACCA_C9HBBANXX_5_20160809B_20160809.bam -F A.sinica.M.head.RNA.01_1.fq -F2 A.sinica.M.head.RNA.01_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaSinica/39902_ACTGAT_C9HBBANXX_5_20160809B_20160809.bam -F A.sinica.M.head.RNA.02_1.fq -F2 A.sinica.M.head.RNA.02_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/Kazakhstan/41809_GATCAG_C9M9GANXX_7_20161014B_20161014.bam -F A.kazakhstan.F.head.RNA.09_1.fq -F2 A.kazakhstan.F.head.RNA.09_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/Kazakhstan/41810_CGTACG_C9M9GANXX_7_20161014B_20161014.bam -F A.kazakhstan.F.head.RNA.10_1.fq -F2 A.kazakhstan.F.head.RNA.10_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/Kazakhstan/41803_ACAGTG_C9MF6ANXX_1_20160830B_20160830.bam -F A.kazakhstan.M.head.RNA.03_1.fq -F2 A.kazakhstan.M.head.RNA.03_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/Kazakhstan/41804_GGCTAC_C9MF6ANXX_1_20160830B_20160830.bam -F A.kazakhstan.M.head.RNA.04_1.fq -F2 A.kazakhstan.M.head.RNA.04_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaUrmiana/61775_AGCGATAGTAATCTTA_CC1HJANXX_3_20180406B_20180406.bam -F A.urmiana.F.head.RNA.75_1.fq -F2 A.urmiana.F.head.RNA.75_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaUrmiana/61779_ATTCAGAAATAGAGGC_CC1HJANXX_2_20180406B_20180406.bam -F A.urmiana.F.head.RNA.79_1.fq -F2 A.urmiana.F.head.RNA.79_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaUrmiana/61773_TCCGCGAAGGCTCTGA_CC1HJANXX_3_20180406B_20180406.bam -F A.urmiana.M.head.RNA.73_1.fq -F2 A.urmiana.M.head.RNA.73_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaUrmiana/61777_TCCGGAGAGTACTGAC_CC1HJANXX_2_20180406B_20180406.bam -F A.urmiana.M.head.RNA.77_1.fq -F2 A.urmiana.M.head.RNA.77_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ParthenogeneticA/NewReads/101424_TAACGATTAATAACGT_CDV98ANXX_3_20191107B_20191107.bam -F A.parAibi.F.head.RNA.424_1.fq -F2 A.parAibi.F.head.RNA.424_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ParthenogeneticA/NewReads/101425_ATGTAGACTTCTTGAA_CDV98ANXX_3_20191107B_20191107.bam -F A.parAibi.F.head.RNA.425_1.fq -F2 A.parAibi.F.head.RNA.425_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaUrmiana/45056_GAGTGG_C9KUHANXX_7_20161021B_20161021.bam -F A.parUrm.F.head.RNA.56_1.fq -F2 A.parUrm.F.head.RNA.56_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/ArtemiaUrmiana/45057_TGACCA_C9KUHANXX_7_20161021B_20161021.bam -F A.parUrm.F.head.RNA.57_1.fq -F2 A.parUrm.F.head.RNA.57_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/Atanasovsko/45191_ACTGAT_C9KRJANXX_7_20161102B_20161102.bam -F A.parAta.F.head.RNA.91_1.fq -F2 A.parAta.F.head.RNA.91_2.fq +

java -jar $PICARD SamToFastq -I /archive3/group/vicosgrp/shared/artemia_RNAseq_reads/Atanasovsko/45192_GTCCGC_C9KRJANXX_7_20161102B_20161102.bam -F A.parAta.F.head.RNA.92_1.fq -F2 A.parAta.F.head.RNA.92_2.fq +
```
</p>
</details>

## 2. Trimming  

<details><summary>code for trimming</summary>
<p>  

```
module load trimmomatic
module load java
cd /nfs/scistore03/vicosgrp/ukhaurat/all_species_males_females_Wsnps

for f in //nfs/scistore03/vicosgrp/ukhaurat/all_species_males_females_Wsnps/*_1.fq
do
base=$(basename ${f%%_*})
srun java -jar /nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 8 
${base}_1.fq ${base}_2.fq 
${base}_1.trim.fq ${base}_1un.trim.fq ${base}_2.trim.fq ${base}_2un.trim.fq 
ILLUMINACLIP:/nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 
SLIDINGWINDOW:4:22 MINLEN:36 LEADING:3 TRAILING:3
done

/usr/bin/time -v sbatch trimming.sh 
Submitted batch job 24751597
```
didn't work, needed to delete tabs and enters! fixed the loop

beforehead tried with individual files, the command worked.
```
module load trimmomatic
cd /nfs/scistore03/vicosgrp/ukhaurat/all_species_males_females_Wsnps/

srun java -jar /nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 4 A.sinica.M.head.RNA.77_1.fasta.gz A.sinica.M.head.RNA.77_2.fasta.gz A.sinica.M.head.RNA.77_1_trimmed.fq A.sinica.M.head.RNA.77_1_un.trimmed.fq A.sinica.M.head.RNA.77_2_trimmed.fq A.sinica.M.head.RNA.77_2_un.trimmed.fq ILLUMINACLIP:/nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 SLIDINGWINDOW:4:22 MINLEN:36 LEADING:3 TRAILING:3

srun java -jar /nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 4 A.sinica.M.head.RNA.78_1.fasta.gz A.sinica.M.head.RNA.78_2.fasta.gz A.sinica.M.head.RNA.78_1_trimmed.fq A.sinica.M.head.RNA.78_1_un.trimmed.fq A.sinica.M.head.RNA.78_2_trimmed.fq A.sinica.M.head.RNA.78_2_un.trimmed.fq ILLUMINACLIP:/nfs/scistore03/vicosgrp/Bioinformatics_2018/Beatriz/0-Software/Trimmomatic-0.36/adapters/TruSeq3-PE.fa:2:30:10 SLIDINGWINDOW:4:22 MINLEN:36 LEADING:3 TRAILING:3

/usr/bin/time -v sbatch trimming_gz.sh
Submitted batch job 24698471
``` 
</p>
</details>
  

## 3. Mapping

<details><summary>code for mapping</summary>
<p>

loop never worked :( smth with naming i guess.  

```
module load star

cd /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.kazakhstan.F.head.RNA.10_1.trim.fq A.kazakhstan.F.head.RNA.10_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.kazakhstan.F.head.RNA.10_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.kazakhstan.M.head.RNA.03_1.trim.fq A.kazakhstan.M.head.RNA.03_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.kazakhstan.M.head.RNA.03_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.kazakhstan.M.head.RNA.04_1.trim.fq A.kazakhstan.M.head.RNA.04_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.kazakhstan.M.head.RNA.04_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.parAibi.F.head.RNA.424_1.trim.fq A.parAibi.F.head.RNA.424_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.parAibi.F.head.RNA.424_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.parAibi.F.head.RNA.425_1.trim.fq A.parAibi.F.head.RNA.425_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.parAibi.F.head.RNA.425_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.parAta.F.head.RNA.91_1.trim.fq A.parAta.F.head.RNA.91_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.parAta.F.head.RNA.91_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.parAta.F.head.RNA.92_1.trim.fq A.parAta.F.head.RNA.92_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.parAta.F.head.RNA.92_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.parUrm.F.head.RNA.56_1.trim.fq A.parUrm.F.head.RNA.56_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.parUrm.F.head.RNA.56_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.parUrm.F.head.RNA.57_1.trim.fq A.parUrm.F.head.RNA.57_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.parUrm.F.head.RNA.57_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.sinica.M.head.RNA.77_1.trim.fq A.sinica.M.head.RNA.77_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.sinica.M.head.RNA.77_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.sinica.M.head.RNA.78_1.trim.fq A.sinica.M.head.RNA.78_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.sinica.M.head.RNA.78_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.sinica.F.head.RNA.69_1.trim.fq A.sinica.F.head.RNA.69_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.sinica.F.head.RNA.69_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.sinica.F.head.RNA.70_1.trim.fq A.sinica.F.head.RNA.70_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.sinica.F.head.RNA.70_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.sinica.F.head.RNA.97_1.trim.fq A.sinica.F.head.RNA.97_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.sinica.F.head.RNA.97_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.sinica.F.head.RNA.98_1.trim.fq A.sinica.F.head.RNA.98_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.sinica.F.head.RNA.98_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.sinica.M.head.RNA.01_1.trim.fq A.sinica.M.head.RNA.01_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.sinica.M.head.RNA.01_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.sinica.M.head.RNA.02_1.trim.fq A.sinica.M.head.RNA.02_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.sinica.M.head.RNA.02_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.urmiana.F.head.RNA.79_1.trim.fq A.urmiana.F.head.RNA.79_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.urmiana.F.head.RNA.79_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.urmiana.M.head.RNA.73_1.trim.fq A.urmiana.M.head.RNA.73_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.urmiana.M.head.RNA.73_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.urmiana.M.head.RNA.77_1.trim.fq A.urmiana.M.head.RNA.77_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.urmiana.M.head.RNA.77_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn A.urmiana.F.head.RNA.75_1.trim.fq A.urmiana.F.head.RNA.75_2.trim.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/A.urmiana.F.head.RNA.75_ --runThreadN 10
```
</p>
</details>  

__mapping statistics__

## 4. SNP calling  

Genome: CHRR_integrated.fa   
__I filtered for quality and coverage >10, but didn't remove multialelic sites!__  

<details><summary>code for VCF file creation</summary>
<p>  

```
module load samtools
module load bcftools
module load vcftools

cd /nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/

#Call SNPs from the BAM alignments
srun bcftools mpileup -a AD,DP,SP -Ou -f CHRR_integrated.fa A.sinica.F.head.RNA.69_Aligned.sortedByCoord.out.bam A.sinica.F.head.RNA.70_Aligned.sortedByCoord.out.bam A.sinica.F.head.RNA.97_Aligned.sortedByCoord.out.bam A.sinica.F.head.RNA.98_Aligned.sortedByCoord.out.bam A.sinica.M.head.RNA.01_Aligned.sortedByCoord.out.bam A.sinica.M.head.RNA.02_Aligned.sortedByCoord.out.bam A.sinica.M.head.RNA.77_Aligned.sortedByCoord.out.bam A.sinica.M.head.RNA.78_Aligned.sortedByCoord.out.bam A.kazakhstan.F.head.RNA.09_Aligned.sortedByCoord.out.bam A.kazakhstan.F.head.RNA.10_Aligned.sortedByCoord.out.bam A.kazakhstan.M.head.RNA.03_Aligned.sortedByCoord.out.bam A.kazakhstan.M.head.RNA.04_Aligned.sortedByCoord.out.bam A.urmiana.F.head.RNA.75_Aligned.sortedByCoord.out.bam A.urmiana.F.head.RNA.79_Aligned.sortedByCoord.out.bam A.urmiana.M.head.RNA.73_Aligned.sortedByCoord.out.bam A.urmiana.M.head.RNA.77_Aligned.sortedByCoord.out.bam A.parAta.F.head.RNA.91_Aligned.sortedByCoord.out.bam A.parAta.F.head.RNA.92_Aligned.sortedByCoord.out.bam A.parAibi.F.head.RNA.424_Aligned.sortedByCoord.out.bam A.parAibi.F.head.RNA.425_Aligned.sortedByCoord.out.bam A.parUrm.F.head.RNA.56_Aligned.sortedByCoord.out.bam A.parUrm.F.head.RNA.57_Aligned.sortedByCoord.out.bam| bcftools call -v -f GQ,GP -mO z -o head.sin.kaz.urm_vcf.gz

#filter for quality and coverage
srun vcftools --gzvcf head.sin.kaz.urm_vcf.gz --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 100 --minDP 10 --maxDP 100 --recode --stdout > head.sin.kaz.urm_filtered.vcf

#Filter 2: remove multiallelic
#bcftools view --max-alleles 2 --exclude-types indels head_asex_raremale_cov5_filtered.vcf > head_asex_raremale_cov5_filtered2.vcf

sbatch snp_calling_sin.kaz.urm.sh 
Submitted batch job 24779195
```
</p>
</details>  

The order of individuals in the VCF file:
```
1. A.sinica.F.head.RNA.69_Aligned.sortedByCoord.out.bam
2. A.sinica.F.head.RNA.70_Aligned.sortedByCoord.out.bam
3. A.sinica.F.head.RNA.97_Aligned.sortedByCoord.out.bam
4. A.sinica.F.head.RNA.98_Aligned.sortedByCoord.out.bam
5. A.sinica.M.head.RNA.01_Aligned.sortedByCoord.out.bam
6. A.sinica.M.head.RNA.02_Aligned.sortedByCoord.out.bam
7. A.sinica.M.head.RNA.77_Aligned.sortedByCoord.out.bam
8. A.sinica.M.head.RNA.78_Aligned.sortedByCoord.out.bam

9. A.kazakhstan.F.head.RNA.09_Aligned.sortedByCoord.out.bam
10. A.kazakhstan.F.head.RNA.10_Aligned.sortedByCoord.out.bam
11. A.kazakhstan.M.head.RNA.03_Aligned.sortedByCoord.out.bam
12. A.kazakhstan.M.head.RNA.04_Aligned.sortedByCoord.out.bam

13. A.urmiana.F.head.RNA.75_Aligned.sortedByCoord.out.bam
14. A.urmiana.F.head.RNA.79_Aligned.sortedByCoord.out.bam
15. A.urmiana.M.head.RNA.73_Aligned.sortedByCoord.out.bam
16. A.urmiana.M.head.RNA.77_Aligned.sortedByCoord.out.bam

17. A.parAta.F.head.RNA.91_Aligned.sortedByCoord.out.bam
18. A.parAta.F.head.RNA.92_Aligned.sortedByCoord.out.bam

19. A.parAibi.F.head.RNA.424_Aligned.sortedByCoord.out.bam
20. A.parAibi.F.head.RNA.425_Aligned.sortedByCoord.out.bam

21. A.parUrm.F.head.RNA.56_Aligned.sortedByCoord.out.bam
22. A.parUrm.F.head.RNA.57_Aligned.sortedByCoord.out.bam
```  

## 5. Check for W-SNPs in unsimplified VCF

Prepare the list of W-SNP coordinates 
working directory: `/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/`
```
cat w_snps_perl_script_full_assembly.txt | awk '{print $1, $2}' > w_snps_coordinates.txt
wc w_snps_coordinates.txt
| 181  362 2548 w_snps_coordinates.txt
mv w_snps_coordinates.txt ../all.species.males.females.Wsnps/
```
working directory: `/nfs/scistore03/vicosgrp/ukhaurat/all.species.males.females.Wsnps/`

```
cat head.sin.kaz.urm_filtered.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head.sin.kaz.urm_filtered.vcf_simple

grep -f w_snps_coordinates.txt head.sin.kaz.urm_filtered.vcf_simple > w_snps_all_species.txt

LG17 59057568 . A G 999 . . GT 0 0 0 0 0 0 0 0 1 1 NA 1 2 2 1 NA 1 2 2 2 2 2
LG17 59057738 . G A 999 . . GT 0 0 0 0 0 0 0 0 1 2 NA 2 1 1 1 NA 1 2 2 2 2 2
LG6 71509373 . C T 480 . . GT 0 0 0 0 0 0 NA 0 1 1 0 0 0 0 0 0 1 1 1 1 1 1
LG6 71663076 . A G 999 . . GT 0 NA 0 0 0 0 0 0 2 2 NA 2 0 0 0 0 2 2 2 2 2 2
LG6 77335125 . G T 767 . . GT 0 0 0 0 0 0 0 0 2 2 2 1 0 0 0 0 1 2 2 2 2 2
LG6 78478436 . C T 999 . . GT 0 0 0 0 0 0 0 0 1 2 NA 2 1 2 2 2 NA 2 2 2 2 2
LG6 78478471 . G T 999 . . GT 0 0 0 0 0 0 0 0 1 2 NA 2 2 2 2 2 2 2 2 2 NA 2
LG6 80088209 . G A 243 . . GT 0 0 0 0 0 0 0 0 1 1 NA 0 1 1 2 2 0 0 0 0 0 0
LG6 84781273 . C T 592 . . GT 0 NA 0 0 0 0 0 0 1 1 NA 0 0 0 0 0 1 1 1 1 1 1
LG6 89149179 . A G 996 . . GT NA 0 0 0 0 0 0 0 1 1 0 1 0 0 0 0 1 2 2 2 2 2
LG6 90659669 . C A 718 . . GT 0 0 0 0 0 0 0 0 1 1 NA 0 0 1 1 1 1 1 1 NA 1 1
LG6 114272754 . C T 999 . . GT NA NA 0 0 0 1 1 1 1 1 0 0 1 1 2 2 2 2 2 2 2 2
LG6 118233256 . G C 860 . . GT 0 0 0 0 0 0 0 0 1 1 NA 0 0 0 0 0 0 2 2 2 2 2
```

```
w_snps_coordinates.txt
sed 's/ /\t/g' w_snps_coordinates.txt > w_snps_coordinates.log

vcftools --vcf head.sin.kaz.urm_filtered.vcf --positions w_snps_coordinates.log --recode --recode-INFO-all --out w_snps.vcf
#doesn't work!

#There is also command
vcftools --snps file_containing_snp_IDs.txt 
#but our snpIDs - coordinates are not unique in the whole genome
```

Maybe if i change filtering i fill find more sites
```
#filter for quality and coverage
srun vcftools --gzvcf head.sin.kaz.urm_vcf.gz --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 5 --max-meanDP 1000 --minDP 5 --maxDP 1000 --recode --stdout > head.sin.kaz.urm_diff_filtered.vcf

sbatch snp_calling_sin.kaz.urm_diff.sh 
Submitted batch job 24850283

cat head.sin.kaz.urm_diff_filtered.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head.sin.kaz.urm_diff_filtered.vcf_simple

grep -f w_snps_coordinates.txt head.sin.kaz.urm_diff_filtered.vcf_simple > w_snps_all_species_diff.txt
wc w_snps_all_species_diff.txt
  47 1457 3557 w_snps_all_species_diff.txt
#more SNPs but still no signal :()
```


