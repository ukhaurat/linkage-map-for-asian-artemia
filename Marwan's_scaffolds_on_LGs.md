## Distribution of Marwan's scaffolds from different datasets among cluster-LGs  
07.07.2020  

Marwan discovered W-associated reads and scaffolds in 4 datasets:   
Dataset1 (comparison of 2 *A.sinica* males and females)  
Dataset2 (comparison of 10 F2 hybrid females and hybrid males from the cross of *A.sinica* male and *A.kazakhstan* female)  
Dataset3 (comparison of a rare male DNA and his parthenogenetic sister from Aibi lake)   
Dataset4 (comparison of an *A.franciscana* female and male))  

--- | #Unique Scaffolds	| #of Sex-linked Scaffolds
--- | --- | --- 
Dataset1 Isofemale | 354 | 20/29
Dataset2 Hybrid	| 102	| 27/29
Dataset3 Aibi	 | 526	| 17/29
Dataset4 Franciscana	| 243 | 11/29  

(Marwan did a bit different filtering for his k-mers this time (I used the results of the previous analysis for Dataset2 [to show the distribution of W scaffolds among cluster-LGs](https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/zw-snps_my_naive_approach.md#mix-with-marwans-results), that time he used 31-mers and selected for the scaffolds containing more than 30% of k-mers sequeces)).  
**This time he used 27-mers, chose only k-mers that are present more than 5 times and the scaffolds that contain more that 50% of k-mers sequences.**

   
    
#### 1. Dataset2 Hybrid  
_the lebel-word for these files is **hybrid**_  
I extracted 102 scaffolds, 49 are common with my set of ZW scaffolds (i have 70)
```
cat Pilon_vs_hybrid.blat.sorted.besthit | awk '{print $14}'| sort| uniq > Pilons_hybrid_Marwan
comm -12  <(sort Pilons_hybrid_Marwan) <(sort zw_scaffolds.txt)
zw_comm_hybrid
```
`Scaffold_LGward_k40_assignmentOver5.txt` - scaffold/LG (more than 5 markers from a scaffold on a LG)
```
cat Scaffold_LGward_k40_assignmentOver5.txt | perl -pi -e 's/\.[0-9]* / /gi' | sort | uniq > Scaffold_LGward_k40_assignmentOver5_simple.txt #to get rid of scaffold.1 .2 ...
awk '{gsub("_pilon", "");print}' Pilons_hybrid_Marwan > Pilons_hybrid_Marwan_simple.txt
grep -f Pilons_hybrid_Marwan_simple.txt Scaffold_LGward_k40_assignmentOver5_simple.txt > Pilons_hybrid_inLGs.txt
cat Pilons_hybrid_inLGs.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forhybridPilons.txt
```

**! In all the graphs #markers must be replaced to #scaffolds (number of scaffolds) !**  

| ![ks](images/Dataset_2_Marwan_s_scaffolds_on_cluster-LGs.png) | <details><summary>CLICK ME to view </summary> ![ks](images/number_of_ZW_markers_from_Marwan_s_scaffolds_on_each_LG.png) </details> | ![ks](images/Dataset_2_only_NonAmbiguous-2.png) |
| :--: | :--: | :--: |
| Results | [Previous results with different filtering!](https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/zw-snps_my_naive_approach.md#mix-with-marwans-results) | [described below](https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/edit/master/Marwan's_scaffolds_on_LGs.md#i-also-looked-for-the-marwans-scaffolds-in-a-list-of-nonambiguous-scaffolds-on-lgs) |  

##### I also looked for the Marwan's scaffolds in a list of NonAmbiguous LGs.  

`Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt` - only scaffolds where these >5 markers are only on one LG
```
grep -f Pilons_hybrid_Marwan_simple.txt Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt > Pilons_hybrid_inLGs_NonAmbig.txt
cat Pilons_hybrid_inLGs_NonAmbig.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forhybridPilons_NonAmbiguous.txt
```
**!The most promising LGs: 2**  
  
  
#### 2. Dataset1 Isafemale (_A.sinica_)  
_the lebel-word for these files is **sinica**_   
I extracted 354 scaffolds, 36 are common with my set of ZW scaffolds (i have 70)

<details><summary>CLICK ME the analogous code2</summary>
<p>

```
cat Pilon_vs_sinica.blat.sorted.besthit | awk '{print $14}'| sort| uniq > Pilons_sinica_Marwan
comm -12  <(sort Pilons_sinica_Marwan) <(sort zw_scaffolds.txt)
zw_comm_sinica
```
`Scaffold_LGward_k40_assignmentOver5.txt` - scaffold/LG (more than 5 markers from a scaffold on a LG)
```
awk '{gsub("_pilon", "");print}' Pilons_sinica_Marwan > Pilons_sinica_Marwan_simple.txt
grep -f Pilons_sinica_Marwan_simple.txt Scaffold_LGward_k40_assignmentOver5_simple.txt > Pilons_sinica_inLGs.txt
cat Pilons_sinica_inLGs.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forsinicaPilons.txt
```
`Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt` - only scaffolds where these >5 markers are only on one LG
```
grep -f Pilons_sinica_Marwan_simple.txt Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt > Pilons_sinica_inLGs_NonAmbig.txt
cat Pilons_sinica_inLGs_NonAmbig.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forsinicaPilons_NonAmbiguous.txt
```
</p>
</details>  

| ![ks](images/Dataset_1_Marwan_s_scaffolds_on_cluster-LGs-3.png) | ![ks](images/Dataset_1_Only_NonAmbiguous-2.png) | 
| :--: | :--: |
| Results | Nonambiguous scaffolds (asigned to one LG) |  

**!The most promising LGs: 2, 24**  
  
  
#### 2. Dataset3 Aibi (_a rare male_)  
_the lebel-word for these files is **aibi**_   
I extracted 526 scaffolds, 33 are common with my set of ZW scaffolds (i have 70)

<details><summary>CLICK ME the analogous code</summary>
<p>

```
cat Pilon_vs_aibi.blat.sorted.besthit | awk '{print $14}'| sort| uniq > Pilons_aibi_Marwan
comm -12  <(sort Pilons_aibi_Marwan) <(sort zw_scaffolds.txt)
zw_comm_aibi
```
`Scaffold_LGward_k40_assignmentOver5.txt` - scaffold/LG (more than 5 markers from a scaffold on a LG)
```
awk '{gsub("_pilon", "");print}' Pilons_aibi_Marwan > Pilons_aibi_Marwan_simple.txt
grep -f Pilons_aibi_Marwan_simple.txt Scaffold_LGward_k40_assignmentOver5_simple.txt > Pilons_aibi_inLGs.txt
cat Pilons_aibi_inLGs.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_foraibiPilons.txt
```
`Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt` - only scaffolds where these >5 markers are only on one LG
```
grep -f Pilons_aibi_Marwan_simple.txt Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt > Pilons_aibi_inLGs_NonAmbig.txt
cat Pilons_aibi_inLGs_NonAmbig.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_foraibiPilons_NonAmbiguous.txt
```
</p>
</details>  

| ![ks](images/Dataset_3_Marwan_s_scaffolds_on_cluster-LGs.png) | ![ks](images/Dataset_3_Only_NonAmbiguous-2.png) | 
| :--: | :--: |
| Results | only Nonambiguous scaffolds (asigned to one LG) |  

**!Less obvious!**  
!The most promising LGs: 2 (1,10,15,24)  

   

   
#### 4. Dataset4 Franciscana (_A.franciscana_)  
_the lebel-word for these files is **fran**_   
I extracted 243 scaffolds, 22 are common with my set of ZW scaffolds (i have 70)

<details><summary>CLICK ME the analogous code</summary>
<p>

```
cat Pilon_vs_fran.blat.sorted.besthit | awk '{print $14}'| sort| uniq > Pilons_fran_Marwan
comm -12  <(sort Pilons_fran_Marwan) <(sort zw_scaffolds.txt)
zw_comm_fran
```
`Scaffold_LGward_k40_assignmentOver5.txt` - scaffold/LG (more than 5 markers from a scaffold on a LG)
```
awk '{gsub("_pilon", "");print}' Pilons_fran_Marwan > Pilons_fran_Marwan_simple.txt
grep -f Pilons_fran_Marwan_simple.txt Scaffold_LGward_k40_assignmentOver5_simple.txt > Pilons_fran_inLGs.txt
cat Pilons_fran_inLGs.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forfranPilons.txt
```
`Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt` - only scaffolds where these >5 markers are only on one LG
```
grep -f Pilons_fran_Marwan_simple.txt Scaffold_LGward_k40_assignmentOver5_NonAmbiguous.txt > Pilons_fran_inLGs_NonAmbig.txt
cat Pilons_fran_inLGs_NonAmbig.txt | awk '{print $2}'| sort |uniq -c > markersnumber_LG_forfranPilons_NonAmbiguous.txt
```
</p>
</details>  


| ![ks](images/Dataset_4_Marwan_s_scaffolds_on_cluster-LGs.png) | ![ks](images/Dataset_4_Only_NonAmbiguous-3.png) | 
| :--: | :--: |
| Results | only Nonambiguous scaffolds (asigned to one LG) |  

**Here is also the most promising LG is the second one (LG 2)**