# Check for W-SNPs in the rare male Aibi genome  
01.09.2020

Check for W-SNPs (got with Betriz perl script [FindZWSNPs_v2.pl](https://git.ist.ac.at/bvicoso/randomdataanalysis/-/blob/master/scripts/FindZWSNPs_v2.pl)) in the rare male genome 

### Preliminary information
**The data I'm using:**  

`/nfs/scistore03/vicosgrp/ukhaurat/w_snps_raremale/head_filtered2.vcf.simple_mbams.ZWassign` - table resulting from Betriz perl script [FindZWSNPs_v2.pl](https://git.ist.ac.at/bvicoso/randomdataanalysis/-/blob/master/scripts/FindZWSNPs_v2.pl) and bam files with merged GM and GF bams  
Structure of the table:  
the following columns (separated by spaces):
* scaffold 
* position 
* grandmother GT (1 = het, 2 = hom)
* number of heterozygous F2 females 
* number of heterozygous F2 males 
* chromosomal assignment (class assignment!)

_the pipeline to find ZW SNPs with the perl script is discribes here: [FindZWsnps_june2020.md](https://git.ist.ac.at/bvicoso/randomdataanalysis/-/blob/master/SlavaLepMap_2020/FindZWsnps_june2020.md)_

### Workflow

#### 1. To create a table of W-SNPs

1. Find the SNPs: heterozygous in GM and found in all the F2 females and in none of the F2 males. Look for markers that are heterozygous in 9 or 10 F2 females and homozygous in all or 9 of the F2 males.  

```
cat head_filtered2.vcf.simple_mbams.ZWassign | awk '(($3==1) && ($4>=9) && ($5<=1))' > w_snps_mynaive.txt
```
138 markers (some autosomal (class 0) got here)

2. Oh! the 6th column was the class assignment, so I can just filter the rows (SNPs) assignt to class 1

_reminder:_  
_We want to know which of our SNPs are sex-linked. Here are the possible chromosomal assignments:_
* class "0" is autosomal (it was not alternatively fixed in males and females)
* class "1" is a W-SNP (it was found in all the F2 females and none of the males, and it was heterozygous in the inbread grandmother)
* class "2" is a ZW-SNP (it was found in all the F2 females and none of the males, but it was homozygous in the ZW grandmother)
* class "3" is Z-specific (it was homozygous in grandmother and not passed on to F2)

```
cat head_filtered2.vcf.simple_mbams.ZWassign | awk '($6==1)' > w_snps_class1.txt
```
113 markers

#### 2. To make a vcf file for the asexual female and rare male

I created a vcf file for asexual Aibi female and rare male Aibi genomes. I used [ADMIXTURE pipeline](https://git.ist.ac.at/bvicoso/bioinformaticsii/-/blob/master/Population%20Genomics/Admixture.md#2-snp-calling-using-samtools-bcftools-and-vcftools)  

Here and there the vcf is filtered:
* for minimum and maximum coverage
* for minimum quality
* indels and multi-allelic sites were removed

```
module load samtools
module load bcftools
module load vcftools

srun bcftools mpileup -a AD,DP,SP -Ou -f AsinicaMpbPilon.fasta asexualsister_Aligned.sortedByCoord.out.bam raremale_Aligned.sortedByCoord.out.bam | bcftools call -v -f GQ,GP -mO z -o head.vcf.gz

srun vcftools --gzvcf head.vcf.gz  --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 10 --max-meanDP 100 --minDP 10 --maxDP 100 --recode --stdout >  head_filtered.vcf

bcftools view --max-alleles 2 --exclude-types indels head_filtered.vcf > head_filtered2.vcf_asex_raremale
```
Then I made a simplified vcf according to [the pipeline](https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/zw-snps_with_Beatrizs_perl_script.md#simplify-the-vcf).  
We can also simplify it so 0/0 is replaced with 0, 0/1 with 1, and 1/1 with 2.
```
cat head_filtered2.vcf_asex_raremale | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_filtered2.vcf.simple_raremale
```
#### 3. To find W-SNPs in the rare male  

try to find w-snps in simplified vcf for asexual female and rare male.
```
grep -f w_snps_class1_scaf_coord.txt head_filtered2.vcf.simple_raremale
```

**I did not find any W_SNPs from this list in the vcf file!**  
From W-linked scaffolds I found only 3 out of 33.  
tig00000030_pilon  
tig00000087_pilon  
tig00000105_pilon    
Maybe the mapping went so bad?   
(77% PE reads survived, [trimming and mapping results](https://docs.google.com/spreadsheets/d/1fJiXKhBuFj9qwXUPU4GpXMAWsGItBW1YUM0IvpS4KcI/edit?usp=sharing)) or filtering is too strict?

I removed the filtering for coverage (removed: --min-meanDP 10 --max-meanDP 100 --minDP 10 --maxDP 100)
```
srun vcftools --gzvcf head.vcf.gz  --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --recode --stdout >  head_filtered_nocov1.vcf
bcftools view --max-alleles 2 --exclude-types indels head_filtered_nocov1.vcf > head_filtered2.vcf_asex_raremale_nocov1
```
then i found some W-SNPs:  

tig00000087_pilon 972007 . G A 41.4279 . . GT 1 2  
tig00000087_pilon 1075860 . A G 73 . . GT 1 2  
tig00000375_pilon 192566 . T C 78 . . GT 1 1  
tig00000375_pilon 193739 . T C 79 . . GT 1 1  
tig00000375_pilon 193804 . T C 77 . . GT 1 1  
tig00000375_pilon 702869 . A T 85 . . GT 1 1  
tig00001910_pilon 143896 . C T 91 . . GT 1 2  
tig00002493_pilon 74757 . A C 223 . . GT 1 1  
tig00002493_pilon 74762 . A T 227 . . GT 1 1  
tig00002493_pilon 129574 . A G 37.5082 . . GT 1 2  
tig00005088_pilon 147093 . A T 82 . . GT 1 2  
tig00373061_pilon 530933 . G C 67 . . GT 1 2  


#### I decided to check for ZW markers (class 2) in vcf for rare male and his sister

```
cat head_filtered2.vcf.simple_mbams.ZWassign | awk '($6==2)' > zw_snps_class2.txt
cat zw_snps_class2.txt | awk '{print $1,$2}' > zw_snps_class2_scaf_coord.txt
grep -f zw_snps_class2_scaf_coord.txt head_filtered2.vcf.simple_raremale

#I found 6 markers:

tig00000030_pilon 203099 . C T 195 . . GT 1 2
tig00000087_pilon 961047 . C T 38.288 . . GT 1 0
tig00000087_pilon 961143 . C A 34.2669 . . GT 1 0
tig00000087_pilon 961324 . G A 30.2296 . . GT 1 0
tig00000087_pilon 961454 . T A 49.2345 . . GT 1 0
tig00000087_pilon 961474 . T G 42.2466 . . GT 1 0
```
And in the female all of them are heterozygous, and in male all of them are homozygous

I checked for zw-snps in a vcf that wasn't filtered for coverage, then i found 47 zw markers `zw_snps_inraremalevcf.txt`.

```
tig00000030_pilon 203099 . C T 195 . . GT 1 2
tig00000030_pilon 203633 . G A 172 . . GT 1 2
tig00000087_pilon 961047 . C T 38.288 . . GT 1 0
tig00000087_pilon 961143 . C A 34.2669 . . GT 1 0
tig00000087_pilon 961324 . G A 30.2296 . . GT 1 0
tig00000087_pilon 961454 . T A 49.2345 . . GT 1 0
tig00000087_pilon 961474 . T G 42.2466 . . GT 1 0
tig00000375_pilon 193691 . G A 194 . . GT 1 1
tig00000375_pilon 701561 . G A 117 . . GT 1 1
tig00000375_pilon 701642 . G T 68 . . GT 1 1
tig00000375_pilon 701819 . A G 78 . . GT 1 1
tig00000375_pilon 702050 . A G 142 . . GT 1 1
tig00000375_pilon 702066 . G T 116 . . GT 1 1
tig00000375_pilon 702196 . T C 92 . . GT 1 1
tig00000375_pilon 702261 . A T 73 . . GT 1 1
tig00001379_pilon 45637 . C A 141 . . GT 1 1
tig00001910_pilon 119451 . A G 78 . . GT 2 1
tig00002281_pilon 317611 . A C 193 . . GT 2 1
tig00002281_pilon 318021 . C G 104 . . GT 1 2
tig00002281_pilon 318040 . C T 122 . . GT 1 2
tig00002281_pilon 318055 . G A 122 . . GT 1 2
tig00002281_pilon 319187 . T C 131 . . GT 1 1
tig00002281_pilon 356700 . C T 159 . . GT 1 1
tig00002281_pilon 357003 . G A 120 . . GT 2 1
tig00002938_pilon 114145 . C T 92 . . GT 1 2
tig00003428_pilon 173594 . C T 130 . . GT 1 2
tig00003428_pilon 290182 . G T 48.1168 . . GT 2 0
tig00005088_pilon 147069 . G C 125 . . GT 1 2
tig00006381_pilon 43206 . C T 150 . . GT 1 2
tig00009620_pilon 49959 . G A 81 . . GT 1 1
tig00009620_pilon 49977 . C T 68 . . GT 1 1
tig00010444_pilon 24125 . T C 31.42 . . GT 1 1
tig00013906_pilon 59005 . C A 36.9218 . . GT 2 0
tig00013943_pilon 21346 . T C 125 . . GT 1 2
tig00015734_pilon 705 . G A 34.6006 . . GT 1 1
tig00015734_pilon 873 . C T 77 . . GT 1 1
tig00015734_pilon 960 . G T 50 . . GT 1 1
tig00015734_pilon 53906 . T C 119 . . GT 1 2
tig00042162_pilon 273917 . C T 52 . . GT 1 1
tig00042162_pilon 273952 . G A 113 . . GT 1 2
tig00373443_pilon 103867 . A T 227 . . GT 1 1
tig00373678_pilon 20499 . A G 104 . . GT 1 1
tig00373678_pilon 20507 . T C 105 . . GT 1 1
tig00373678_pilon 20619 . C T 47.1371 . . GT 0 1
tig00373678_pilon 27867 . G A 132 . . GT 1 1
tig00373678_pilon 35263 . C G 166 . . GT 1 1
```
tig00374113_pilon 14031 . T G 78 . . GT 1 2
