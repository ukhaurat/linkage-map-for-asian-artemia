## Figure for the Artemia paper

#### We want to make a figure which illustrates the data on the Z chromosome for four Artemia species: franciscana, kazakhstan, urmiana, and sinica.

I made a visualisation:
![figure_1_visualisation](images/Screenshot_2021-05-13_at_22.12.12.png)

working directory: /nfs/scistore03/vicosgrp/ukhaurat/figure_1_Artemia  
the assembly of the Z chromosome (A.sinica) Marwan gave me: z_final_cds.fasta

[Andrea's pipeline for the Fst calculation in a sliding window](https://git.ist.ac.at/amrnjava/mf-fst/-/blob/master/male%20female%20Fst.md)

```
cat z_final_cds.fasta | awk '{if(NR%4==2) print length($1)}' | sort -n | uniq -c > z_final_cds.txt

reads<-read.csv(file="read_length.txt", sep="", header=FALSE)
plot (reads$V2,reads$V1,type="l",xlab="read length",ylab="occurences",col="blue")
```
[Vincent's pipeline for the coverage calculation](https://git.ist.ac.at/vbett/artemia-genomic-species/-/blob/master/pipeline%20for%20coverage%20analysis%20using%20sliding%20window%20after%20mapping%20to%20male%20masked%20genome%20to%20identify%20Z%20chromosome%20differentiation)
