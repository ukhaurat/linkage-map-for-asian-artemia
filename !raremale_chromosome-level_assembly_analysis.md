# Repeat the analysis for rare male sex chromosomes and it's asexual sister with the chromosome-level genome assembly

## 1. Map raremale DNA and its asexual sister to the new full genome 

- make links of files with trimmed reads
cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/
ln -s /nfs/scistore03/vicosgrp/ukhaurat/data/trimmed_raremale_DNA/*fq ./

```
STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn asexualsister_Aibi_1_trimmed.fq asexualsister_Aibi_2_trimmed.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/asexualsister_Aibi_ --runThreadN 10

STAR --genomeDir /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/index/ --readFilesIn raremale_Aibi_1_trimmed.fq raremale_Aibi_2_trimmed.fq --outSAMtype BAM SortedByCoordinate --outFileNamePrefix /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/raremale_Aibi_ --runThreadN 10

sbatch star_raremale.sh 
Submitted batch job 24431037
```
- vcf creation with the less strict filtering for coverage. 
(here)[https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/5.raremale_w-snps_new_genome.md#2a-to-make-a-vcf-file-filer-for-coverage-5]

```
cd /nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/

#Call SNPs from the BAM alignments
srun bcftools mpileup -a AD,DP,SP -Ou -f CHRR_integrated.fa asexualsister_Aibi_Aligned.sortedByCoord.out.bam raremale_Aibi_Aligned.sortedByCoord.out.bam | bcftools call -v -f GQ,GP -mO z -o head_asex_raremale.vcf.gz

#filter for quality and coverage
srun vcftools --gzvcf head_asex_raremale.vcf.gz --remove-indels --maf 0.1 --max-missing 0.9 --minQ 30 --min-meanDP 5 --max-meanDP 100 --minDP 5 --maxDP 100 --recode --stdout > head_asex_raremale_cov5_filtered.vcf

#Filter 2: remove multiallelic
bcftools view --max-alleles 2 --exclude-types indels head_asex_raremale_cov5_filtered.vcf > head_asex_raremale_cov5_filtered2.vcf

sbatch snp_calling_asex_raremale.sh 
Submitted batch job 24431832
```

create a simplified vcf:
```
cat head_asex_raremale_cov5_filtered2.vcf | grep -v '^##' | perl -pi -e 's/:.*?\t/\t/gi' | perl -pi -e 's/:.*//gi' | perl -pi -e 's/\t/ /gi' | perl -pi -e 's/0\/0/0/gi' | perl -pi -e 's/1\/1/2/gi' | perl -pi -e 's/0\/1/1/gi' | perl -pi -e 's/\.\/\./NA/gi' > head_asex_raremale_cov5_filtered2.vcf_simple
```
why is it so big (100 858 148)? in comparisson with `head_Gk1_Gs1_f_m_filtered2.vcf_simple` (10 064 250 bytes). oh! probably it is because DNA was used.

## 2. Find W-SNPs and check for it in the raremale-asex VCF 

What W and ZW SNPs we detected in the full-chromosonmal-level assembly?   

`/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis`
```
cat head_Gk1_Gs1_f_m_filtered2.vcf_simple.ZWassign | awk '{print $6}' | sort | uniq -c | awk '{print $2, $1}'
0 54841
1 181
2 820
```
181 W-SNPs, more than in the previous!
```
0 [autosome] 48673
1 [w] 144
2 [zw] 706
```
Prepare the list of W-SNP coordinates 
```
cat w_snps_perl_script_full_assembly.txt | awk '{print $1, $2}' > w_snps_coordinates.txt
wc w_snps_coordinates.txt
| 181  362 2548 w_snps_coordinates.txt
```
Check for the W-SNPs in the raremale-asex VCF, we found 32 W-SNPs (last time it was 24)
```
grep -f w_snps_coordinates.txt head_asex_raremale_cov5_filtered2.vcf_simple > w_snps_raremale.txt
ukhaurat@bea81:~/full_assembly_analysis$ wc w_snps_raremale.txt
  32  352 1142 w_snps_raremale.txt
```

## 3. Find SNPs with the lost heterozygosity in the raremale genome

```
# do it from the computer
scp -o ProxyJump=ukhaurat@login.ist.ac.at ukhaurat@bea81:/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/head_asex_raremale_cov5_filtered2.vcf_simple /Users/ukhaurat/Documents/full_assembly_analysis/
```
The Rmd file with the R code  
- to plot fraction of SNPs with the lost heterozygosity on each chromosome   
- to get the files with the best scaffold candidates for the recombined region in the rare male    

__[Rmd file](https://git.ist.ac.at/ukhaurat/linkage-map-for-asian-artemia/-/blob/master/files/second_raremale_heterozygosity.Rmd)__
     

????     
```
scp -o ProxyJump=ukhaurat@login.ist.ac.at ukhaurat@bea81:/nfs/scistore03/vicosgrp/ukhaurat/full_assembly_analysis/w_snps_perl_script_full_assembly.txt /Users/ukhaurat/Documents/full_assembly_analysis/
```

